package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import org.json.JSONObject;
import org.mortbay.util.ajax.JSON;

import java.util.*;

public class TwitterReducerJson extends Reducer<Text, Text, Text, Text> {

    /**
     *
     * @param key userID
     * @param values tab-separated useful tweet info
     * @param con context
     */
    @Override
    public void reduce(Text key, Iterable<Text> values, Context con) throws InterruptedException, IOException {
        JSONObject jO = new JSONObject();
        List<String> duplicateId = new ArrayList<>();
        for (Text value : values) {
            // only takes one
            String[] content = value.toString().split("\t");
            String Original = content[3].replaceAll("teamcc150thebest", "\t");

            String noURL = urlRemoved(Original);  // Filtered out URL
            int EWC = getImpactScore(getEffectiveWords(noURL));  // gets the effective word count
            int frfCount = Integer.parseInt(content[4]);
            long impactScore = 0;
            if (frfCount > 0) {
                impactScore = EWC * frfCount;
            }

            String censoredFulltext = getCensored(Original);
            JSONObject output = new JSONObject();
            output.put("tweet_id", content[0]);
            output.put("user_id", content[1]);
            output.put("created_at", content[2]);
            output.put("censored_text", censoredFulltext);
            output.put("impactScore", impactScore);
            output.put("noURL", noURL);
            con.write(new Text(content[0] +"\t"+ key.toString()), new Text(output.toString()));
            break;
        }
    }

    private String urlRemoved(String tweetText) {
        return tweetText.replaceAll("(https?|ftp)://[^\\t\\r\\n /$.?#][^\\t\\r\\n ]*", "");
    }

    private Integer getImpactScore(ArrayList<String> tweetText) {
        if (tweetText.size() != 0) {
            int counter = 0;
            for (int i=0; i < tweetText.size(); i++) {
                String filtered = tweetText.get(i).replaceAll("[^A-Za-z]", "");
                if (filtered.length() != 0 && !Arrays.asList(stopWords).contains(tweetText.get(i).toLowerCase(Locale.ENGLISH))) {
                    counter += 1;
                }
            }
            return counter;
        } else {
            return 0;
        }
    }

    private ArrayList<String> getEffectiveWords(String tweetText) {
        if (!tweetText.equals("")) {
            String[] wordSep = tweetText.split("[^A-Za-z0-9'-]");
            ArrayList<String> aL = new ArrayList<String>();
            for (String word : wordSep) {
                String filtered = word.replaceAll("[^A-Za-z]", "");  // checks if only numbers
                if (filtered.length() != 0 && !Arrays.asList(stopWords).contains(word.toLowerCase(Locale.ENGLISH))) {
                    aL.add(word);
                }
            }
            return aL;
        } else {
            return new ArrayList<>(0);
        }
    }

    private String getCensored(String tweetText) {
        if (!tweetText.equals("")) {
            String[] textSplit = tweetText.split("[^A-Za-z0-9]");
            for (String element : textSplit) {
                System.out.println(element);
            }
            for (String text : textSplit) {
                if (text.length() != 0 && Arrays.asList(censorWords).contains(text.toLowerCase(Locale.ENGLISH))) {
                    ArrayList<Integer> posList = getPosition(tweetText.toLowerCase(Locale.ENGLISH), text.toLowerCase(Locale.ENGLISH));
                    for (int i = 0; i < posList.size(); i++) {
                        Integer pos = posList.get(i);
                        if (pos == 0) {
                            if (pos + text.length() == tweetText.length()) {
                                // if the word spans the entire length of tweetText
                                tweetText = muteString(tweetText, text, pos);
                            } else {
                                if (tweetText.substring(pos + text.length(), pos + text.length() + 1).matches("[^A-Za-z0-9]")) {
                                    tweetText = muteString(tweetText, text, pos);
                                } else {
                                    assert true;  // does nothing
                                }
                            }
                        } else if (pos + text.length() == tweetText.length()) {
                            // word reaches the end of tweetText, checks start of word
                            if (tweetText.substring((pos - 1), pos).matches("[^A-Za-z0-9]")) {
                                tweetText = muteString(tweetText, text, pos);
                            } else {
                                assert true;  // does nothing
                            }
                        } else {
                            // both ends of the word are needed to be checked
                            if (tweetText.substring((pos - 1), pos).matches("[^A-Za-z0-9]") && tweetText.substring(pos + text.length(), pos + text.length() + 1).matches("[^A-Za-z0-9]")) {
                                tweetText = muteString(tweetText, text, pos);
                            } else {
                                assert true;  // does nothing
                            }
                        }
                    }
                }
            }
            return tweetText;
        } else {
            return "";
        }
    }

    private ArrayList<Integer> getPosition(String tweetText, String word) {
        ArrayList<Integer> posList = new ArrayList<>();
        int index = tweetText.indexOf(word);
        while (index >= 0) {
            posList.add(index);
            index = tweetText.indexOf(word, index + 1);
        }
        return posList;
    }

    private String muteString(String tweetText, String word, Integer pos) {
        StringBuilder sb = new StringBuilder();
        String star = "*";
        int n = word.length() - 2;
        for(int i=0; i<n;i++){
            sb.append(star);
        }
        return tweetText.substring(0, pos+1) + sb.toString() + tweetText.substring(pos+word.length() - 1);
    }
    private static final String[] censorWords = {"15619cctest", "4r5e", "5h1t", "5hit", "n1gga", "n1gger", "nobhead", "nobjocky", "nobjokey", "nutsack", "numbnuts", "nazi", "nigg3r", "nigg4h", "nigga", "niggas", "niggaz", "niggah", "nigger", "niggers", "omg", "p0rn", "poop", "pron", "prick", "pricks", "pussy", "pussys", "pusse", "pussi", "pussies", "pube", "pawn", "penis", "penisfucker", "phonesex", "phuq", "phuck", "phuk", "phuks", "phuked", "phuking", "phukked", "phukking", "piss", "pissoff", "pisser", "pissers", "pisses", "pissflaps", "pissin", "pissing", "pigfucker", "pimpis", "queer", "rectum", "rimjaw", "rimming", "snatch", "sonofabitch", "spunk", "scrotum", "scrote", "scroat", "schlong", "sex", "semen", "sh1t", "shit", "shits", "shitty", "shitter", "shitters", "shitted", "shitting", "shittings", "shitdick", "shite", "shitey", "shited", "shitfuck", "shitfull", "shithead", "shiting", "shitings", "skank", "slut", "smut", "smegma", "tosser", "turd", "tw4t", "twunt", "twunter", "twat", "twatty", "twathead", "teets", "tit", "tittywank", "tittyfuck", "titties", "tittiefucker", "titwank", "titfuck", "v14gra", "v1gra", "vulva", "vagina", "viagra", "w00se", "wtff", "wang", "wank", "wanky", "wanker", "whore", "whore4r5e", "whoreshit", "whoreanal", "whoar", "a55", "anus", "anal", "arse", "ass", "assram", "asswhole", "assfucker", "assfukka", "assho", "b00bs", "b17ch", "b1tch", "boner", "booooooobs", "booooobs", "boooobs", "booobs", "boob", "boobs", "boiolas", "bollock", "bollok", "breasts", "bunnyfucker", "butt", "buttplug", "buttmuch", "buceta", "bugger", "bullshit", "bum", "bastard", "balls", "ballsack", "bestial", "bestiality", "beastial", "beastiality", "bellend", "bitch", "biatch", "bloody", "blowjob", "blowjobs", "c0ck", "c0cksucker", "cnut", "coon", "cox", "cock", "cocks", "cocksuck", "cocksucks", "cocksucker", "cocksucked", "cocksucking", "cocksuka", "cocksukka", "cockface", "cockhead", "cockmunch", "cockmuncher", "cok", "coksucka", "cokmuncher", "crap", "cunnilingus", "cunt", "cunts", "cuntlick", "cuntlicker", "cuntlicking", "cunilingus", "cunillingus", "cum", "cums", "cumshot", "cummer", "cumming", "cyalis", "cyberfuc", "cyberfuck", "cyberfucker", "cyberfuckers", "cyberfucked", "cyberfucking", "carpetmuncher", "cawk", "chink", "cipa", "cl1t", "clit", "clitoris", "clits", "d1ck", "donkeyribber", "doosh", "dogfucker", "doggin", "dogging", "duche", "dyke", "damn", "dink", "dinks", "dirsa", "dick", "dickhead", "dildo", "dildos", "dlck", "ejaculate", "ejaculates", "ejaculated", "ejaculating", "ejaculatings", "ejaculation", "ejakulate", "f4nny", "fook", "fooker", "fux", "fux0r", "fuck", "fucks", "fuckwhit", "fuckwit", "fucka", "fucker", "fuckers", "fucked", "fuckhead", "fuckheads", "fuckin", "fucking", "fuckings", "fuckingshitmotherfucker", "fuckkkdatttbitchhhh", "fuckme", "fudgepacker", "fuk", "fuks", "fukwhit", "fukwit", "fuker", "fukker", "fukkin", "fannyfucker", "fannyflaps", "fanyy", "fag", "fagot", "fagots", "fags", "faggot", "faggs", "fagging", "faggitt", "fcuk", "fcuker", "fcuking", "feck", "fecker", "felching", "fellate", "fellatio", "fingerfuck", "fingerfucks", "fingerfucker", "fingerfuckers", "fingerfucked", "fingerfucking", "fistfuck", "fistfucks", "fistfucker", "fistfuckers", "fistfucked", "fistfucking", "fistfuckings", "flange", "goatse", "goddamn", "gangbang", "gangbangs", "gangbanged", "gaysex", "horny", "horniest", "hore", "hotsex", "hoar", "hoare", "hoer", "homo", "hardcoresex", "heshe", "hell", "jap", "jackoff", "jerk", "jerkoff", "jism", "jiz", "jizz", "jizm", "knob", "knobend", "knobead", "knobed", "knobhead", "knobjocky", "knobjokey", "kondum", "kondums", "kock", "kunilingus", "kum", "kums", "kummer", "kumming", "kawk", "l3itch", "l3ich", "lust", "lusting", "labia", "lmao", "lmfao", "m0f0", "m0fo", "m45terbate", "mothafuck", "mothafucks", "mothafucka", "mothafuckas", "mothafuckaz", "mothafucker", "mothafuckers", "mothafucked", "mothafuckin", "mothafucking", "mothafuckings", "motherfuck", "motherfucks", "motherfucker", "motherfuckers", "motherfucked", "motherfuckin", "motherfucking", "motherfuckings", "motherfuckka", "mof0", "mofo", "mutha", "muthafuckker", "muthafecker", "muther", "mutherfucker", "muff", "ma5terb8", "ma5terbate", "masochist", "masturbate", "masterb8", "masterbat", "masterbat3", "masterbate", "masterbation", "masterbations"};
    private static final String[] stopWords = {"a", "about", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "aren't", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "can't", "co", "computer", "con", "could", "couldnt", "couldn't", "cry", "de", "describe", "detail", "didn't", "do", "doesn't", "done", "don't", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "hadn't", "has", "hasnt", "hasn't", "have", "haven't", "he", "he'd", "he'll", "hence", "her", "here", "hereafter", "hereby", "herein", "here's", "hereupon", "hers", "herself", "he's", "him", "himself", "his", "how", "however", "how's", "hundred", "i", "i'd", "ie", "if", "i'll", "i'm", "in", "inc", "indeed", "interest", "into", "is", "isn't", "it", "its", "it's", "itself", "i've", "keep", "last", "latter", "latterly", "least", "less", "let's", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "mustn't", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "rt", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "that's", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "there's", "thereupon", "these", "they", "they'd", "they'll", "they're", "they've", "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "wasn't", "we", "we'd", "well", "we'll", "were", "we're", "weren't", "we've", "what", "whatever", "what's", "when", "whence", "whenever", "when's", "where", "whereafter", "whereas", "whereby", "wherein", "where's", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "who's", "whose", "why", "why's", "will", "with", "within", "without", "won't", "would", "wouldn't", "yet", "you", "you'd", "you'll", "your", "you're", "yours", "yourself", "yourselves", "you've"};
}