package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.util.StringJoiner;
import org.json.*;

public class TwitterMapper extends Mapper<Object, Text, Text, Text> {
    @Override
    public void map(Object key, Text value, Context context) {

        // Convert the input line to a JSON Object
        String valueString = value.toString();
        try {
            JSONObject tweetObject = new JSONObject(valueString);
            if (DataFilter.checkAllRules(tweetObject)) {
                // Constructs the Tweet Output string
                StringJoiner joiner1 = new StringJoiner("\t");
                String tweetId = DataFilter.getTweeterObjId(tweetObject);
                String userId = DataFilter.getUserObjId(tweetObject);
                long CreatedAt = DataFilter.getUnixDate(DataFilter.getCreated(tweetObject));
                String tweetContent = DataFilter.getText(tweetObject).replaceAll("\t", "teamcc150thebest");

                // debug purposes
                if (tweetId.equals("448460130107944960")) {
                    // System.out.println(valueString);
                }

                int fav = DataFilter.getFavorites(tweetObject);  // favorites count
                int rtc = DataFilter.getRetweetCount(tweetObject);  // retweets count
                int followc = DataFilter.getFollowers(tweetObject);  // followers count
                int total = fav + rtc + followc;
                joiner1.add(tweetId);  // 0
                joiner1.add(userId);   // 1
                joiner1.add(Long.toString(CreatedAt));  // 2
                joiner1.add(tweetContent);  // 3
                joiner1.add(Integer.toString(total));  // 4

                context.write(new Text(tweetId), new Text(joiner1.toString()));
//                // Get censoredTags, return "" if no hashtag found
//                ArrayList censoredTags = DataFilter.getCensoredtag(tweetObject);  // Get censored hashtag for contacts
//                // Get uncensoredTags
//                ArrayList uncensoredTags = DataFilter.getHashtag(tweetObject);

//                // Constructs the User Output string
//                joiner1.add(userId);
//                joiner1.add(DataFilter.getScreenname(tweetObject).replaceAll("\t", "teamcc150thebest"));
//                joiner1.add(DataFilter.getDescription(tweetObject).replaceAll("\t", "teamcc150thebest"));
//                if (!censoredTags.isEmpty()) {
//                    joiner1.add(String.join(",", censoredTags));
//                } else {
//                    joiner1.add("");
//                }
//                joiner1.add(peerId);
            }
        } catch (Exception e) {
            // Ignores Exception, JSON conversion error
        	e.printStackTrace();
        }
    }
}
