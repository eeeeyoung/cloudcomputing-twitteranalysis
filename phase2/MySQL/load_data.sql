-- Step 1 create the database
drop database if exists twitter_db;
create database twitter_db;
use twitter_db;

-- Step 2 create the tweets table
drop table if exists `tweets`;
create table `tweets` (
    `uid` bigint(20) default 0 not null,
    `timestamp` bigint(20) default 0 not null,
    `data` LONGTEXT
 );

-- Step 3 create an index
create index tweets_index ON tweets(uid, timestamp);

-- Step 4 load data to the tweets table
load data local infile 'q3.tsv' into table tweets character set utf8mb4 columns terminated by '\t' ESCAPED by '' LINES TERMINATED BY '\n';
