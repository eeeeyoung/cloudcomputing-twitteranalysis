package edu.cmu.cc150.tweet;

public class Scores implements Comparable<Scores> {
    public String user;
    public double score;
    public long id;

    public Scores(String user, double score) {
        this.user = user;
        this.score = score;
        this.id = Long.parseLong(user);
    }

    @Override
    public int compareTo(Scores other) {
        if (other.score > this.score) {
            return 1;
        } 
        if (other.score == this.score) {
            if(this.id > other.id) return -1;
            else if(this.id < other.id) return 1;
            else return 0;
        }
        return -1;
    }
}
