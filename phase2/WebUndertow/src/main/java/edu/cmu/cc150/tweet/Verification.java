package edu.cmu.cc150.tweet;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.zip.Inflater;
import org.apache.commons.codec.binary.Base64;
import org.json.*;

public class Verification {
	public static BigInteger n = BigInteger.valueOf(Long.parseLong("1284110893049"));
	public static int e = 15619;
	public static BigInteger d = BigInteger.valueOf(Long.parseLong("379666662787"));
	
	/**
	 * base64_decode - decode base64-encoded request and decompress it
	 * 
	 * @param baseString - base64 encoded request
	 * @return parsed JSON-format request 
	 */
	public static JSONObject base64_decode(String baseString) {
		try {
			byte[] compressedData = Base64.decodeBase64(baseString);
			Inflater decompresser = new Inflater();
			int compressedDataLength = compressedData.length;
			decompresser.setInput(compressedData, 0, compressedDataLength);
			byte[] decompressedData = new byte[3000];
		    int decompressedDataLength = decompresser.inflate(decompressedData);
		    if (decompressedDataLength > 3000) {
		    	System.out.println("decompressed data length too long!");
		    }
		    String decompressedStr = new String(decompressedData);
		    JSONObject reqJson = new JSONObject(decompressedStr);
		    return reqJson;
		} catch (java.util.zip.DataFormatException ex) {
			ex.printStackTrace();
		    return null;
		} 
	}
	
	/**
	 * SHA256 - get hash by sha256
	 * 
	 * @param str - str to encrypt
	 * @return hash of str by sha256
	 * @throws NoSuchAlgorithmException
	 */
	public static String SHA256(String str) throws NoSuchAlgorithmException {
		MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
		msgDigest.update(str.getBytes());
		byte[] digest = msgDigest.digest();
		BigInteger bigInt = new BigInteger(1, digest);
		String hex = String.format("%064x", bigInt);
		return hex;
	}
	
	/**
	 * CCHash - get first 8 chars of hash by sha256
	 * 
	 * @param str - str to encrypt
	 * @return first 8 chars of hash by sha256
	 * @throws NoSuchAlgorithmException
	 */
	public static String CCHash(String str) throws NoSuchAlgorithmException {
		return SHA256(str).substring(0, 8);
	}
	
	/**
	 * checkHash - compute CChash given the parameters
	 * 
	 * @param timestamp
	 * @param sender
	 * @param recipient
	 * @param amount
	 * @param fee
	 * @return CChash given the parameters
	 * @throws NoSuchAlgorithmException
	 */
	public static String checkHash(String timestamp, String sender, String recipient, String amount, String fee) 
			throws NoSuchAlgorithmException {
		String str = timestamp + "|" +sender + "|" + recipient + "|" + amount + "|" + fee;
		return CCHash(str);
	}
	
	public static boolean checkSig(long send, String hash, long sig) {
		BigInteger hashInt = new BigInteger(hash, 16);
		BigInteger sigBig = BigInteger.valueOf(sig);
		BigInteger sendBig = BigInteger.valueOf(send);
		BigInteger eBig = BigInteger.valueOf(e); 
		BigInteger res = sigBig.modPow(eBig, sendBig);
		if (res.equals(hashInt)) {
			return true;
		} else {
			return false;	
		}
	}
	
	public static String checkLastHash(String id, String prev, ArrayList<String> list, String pow) {
		String str = id + "|" + prev;
		for (int k = 0; k < list.size(); k++) {
			str = str + "|" + list.get(k);
		}
		String genHash = "";
		try {
			genHash = SHA256(str);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return genHash + pow;
	}
	
	public static boolean verify(JSONObject request) {
		JSONObject newTx = request.getJSONObject("new_tx");
		long newTime = 0;
		try {
			newTime = Long.parseLong(newTx.getString("time"));	
		} catch (Exception e) {
			e.printStackTrace();
		}
		long newAmt = newTx.getLong("amt");
		HashMap<Long, Long> accounts = new HashMap<Long, Long>();
		JSONArray chain = request.getJSONArray("chain");
		ArrayList<Long> timestamps = new ArrayList<Long>();
		int idCounter = 0;
		String prevHash = "00000000";
		for (int i = 0; i < chain.length(); i++) {
			JSONObject transx = chain.getJSONObject(i);
			JSONArray allTx = transx.getJSONArray("all_tx");
			ArrayList<String> hashList = new ArrayList<String>();
			String hash = "";
			long fees = 0;
			for (int j = 0; j < allTx.length(); j++) {
				JSONObject trans = allTx.getJSONObject(j);
				if (trans.length() == 4) {
					// reward transaction
					long recv = trans.getLong("recv");
					long amt = trans.getLong("amt");
					long time = 0;
					try {
						time = Long.parseLong(trans.getString("time"));
					} catch (Exception e) {
						e.printStackTrace();
					}
					hash = trans.getString("hash");
					if (timestamps.size() == 0) {
						timestamps.add(time);
					} else {
						if (time <= timestamps.get(timestamps.size() -1)) {
							//System.out.println("time error");
							return false;
						}
					}
					
					if (amt > 500000000) 
						return false;
					if (amt < 0)
						return false;
					if (accounts.containsKey(recv)) {
						accounts.put(recv, accounts.get(recv) + amt + fees);
					} else {
						accounts.put(recv, amt + fees);
					}
					
					try {
						if (!checkHash(Long.toString(time), "", Long.toString(recv), Long.toString(amt), "").equals(hash)) {
							return false;
						} 
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if (trans.length() == 7) {
					long fee = trans.getLong("fee");
					long send = trans.getLong("send");
					long recv = trans.getLong("recv");
					long amt = trans.getLong("amt");
					long time = 0;
					try {
						time = Long.parseLong(trans.getString("time"));
					} catch (Exception e) {
						e.printStackTrace();
					}

					hash = trans.getString("hash");
					long sig = trans.getLong("sig");
					
					if (!accounts.containsKey(send)) {
						accounts.put(send, (long) 0);
					}
					accounts.put(send, accounts.get(send) - fee);
					if (accounts.get(send) < 0) {
						return false;
					}
					if(amt < 0) {
						return false;
					}
					
					if (!checkSig(send, hash, sig)) {
						return false;
					}
					
					try {
						if (!checkHash(Long.toString(time), Long.toString(send), Long.toString(recv), Long.toString(amt), Long.toString(fee)).equals(hash)) {
							return false;
						} 
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if (timestamps.size() == 0) {
						timestamps.add(time);
					} else {
						if (time <= timestamps.get(timestamps.size() -1)) {
							return false;
						}
						timestamps.add(time);
					}
					
					if (fee < 0) {
						return false;
					}
				
					if (!hash.matches("-?[0-9a-fA-F]+")) {
						return false;
					}
					
					accounts.put(send, accounts.get(send) - amt);
					if (accounts.get(send) < 0) {
						return false;
					}
					
					if (accounts.containsKey(recv)) {
						accounts.put(recv, accounts.get(recv) + amt);
					} else {
						accounts.put(recv, amt);
					}
					
					fees = fees + fee;
				} else {
					return false;
				}
				hashList.add(hash);
			}
			int id = transx.getInt("id");
			String lastHash = transx.getString("hash");
			//System.out.println(lastHash);
			if (lastHash.charAt(0) != '0') {
				return false;
			}
			String pow = transx.getString("pow");
			String hash1 = checkLastHash(Integer.toString(id), prevHash, hashList, pow);
			String genHash = "";
			try {
				genHash = CCHash(hash1);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (!genHash.equals(lastHash)) {
				return false;
			}
			
			if (id != idCounter) {
				System.out.println("id error");
				return false;
			} else {
				idCounter++;
			}
			prevHash = lastHash;	
		}
		
		if (newAmt < 0) {
			return false;
		} 
		if (newTime <= timestamps.get(timestamps.size() -1)) {
			return false;
		}
		if (accounts.containsKey(n.longValue())) {
			if (accounts.get(n.longValue()) < newAmt) {
				return false;
			}
		} else {
			if (newAmt > 0) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) throws NoSuchAlgorithmException {
		JSONObject req = base64_decode("eJzNld2O2jAQhV8F5Xov5s8zdl-lqioncQoSCxUgttWKd--kUHDoZkt71VxZx4n8zZnjyWvTLfNq03xYfHxt8nr9-fDtvN6V7uirKIRiGpUAnxZNfj64GODyuHJYPReXGgwBjDAZQsKoYKyNby_zfjluJ-hk6PrQnD65uupdg2obDIsNKY-ffN2-jNJxtV3nQ2lOT4sp2b5s-t_JLrxqLiGiaLriJgvgyOLCUEZYNjCbZY8RKdbsbRriwKUdtf3qi0si6BVqCBLpJ9-FaXr6exZqIjK-EqlQnANKFEgmQCjFeALk7ytjBCX-L3kuR6dk3gwCJvmLNKVAdx3BGKINsa_ShHWa2sg5dFSlqd1ld2QuS3dcv2iDBUYTSBUtOZWCiF69mudWkzjhLkh95ra7GZXMRDG4-wHrzr2dbgbkqJYYE9_iDWPAkR4ASmBRaqBMQVMOcgMyVCYGDQZQA71tUfQ7EMkhJdwsEhZS_bNBXhoHnRiksQd1T248EZk1UlRWrKPk9nASEAyWHo7SiB-Q6hN70uzdhCpKVEepL72257hfotRtN8ey25f199k83cG9M5t8VIrfJ8IH3BKfqVCzk5QuZ666h54NVi8RGGq3_mmMk6eA4uTEUnQQ7NrKLa7d0s7MBpq6dfAfTNmdP9mUl7NZt0YSiwQvLohd0eaRjM1nwen0A8IhnmI=");
		//System.out.println(req);
		if (verify(req) == true) {
			System.out.println("hello!");
		}
	}
}
