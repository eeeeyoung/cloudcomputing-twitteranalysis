package edu.cmu.cc150.tweet;

public class Tweet implements Comparable<Tweet> {
    private String TweetId;
    private Long score;

    public Tweet (String word, Long score) {
        this.TweetId = word;
        this.score = score;
    }

    @Override
    public int compareTo(Tweet other) {
        if (this.score < other.score) {
            return 1;
        }
        if (this.score.equals(other.score)) {
            long diff = Long.parseLong(this.TweetId) - Long.parseLong(other.TweetId);
            if (diff > 0) {
                // this > other
                return -1;
            } else {
                return 1;
            }
        }
        return -1;
    }

    public String getTweetId() {
        return this.TweetId;
    }

    public Long getScore() {
        return this.score;
    }
}
