package edu.cmu.cc150.tweet.query2;

public class UserInfo {
	public String censoredHashtags;
	public String latestName;
	public String latestDesc;
	public String who;
}
