package edu.cmu.cc150.tweet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import edu.cmu.cc150.tweet.query2.ContactsResult;
import edu.cmu.cc150.tweet.query2.Query2Helper;
import edu.cmu.cc150.tweet.query2.Score;

public class HBaseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/** the private IP address(es) of HBase zookeeper nodes */
	private static String zkPrivateIPs = System.getenv("zkPrivateIPs");
	private static String hbaseMasterPort = System.getenv("hbaseMasterPort");
	private static Table contactsTable;
	private static Gson gson = new Gson();
	private static Connection connection = null;
	private static final Logger LOGGER = Logger.getRootLogger();

	/**
	 * Initialize SQL connection.
	 *
	 * @throws ClassNotFoundException when an application fails to load a class
	 * @throws SQLException           on a database access error or other errors
	 * @throws IOException
	 */
	public HBaseServlet() throws ClassNotFoundException, SQLException, IOException {
		//LOGGER.setLevel(Level.INFO);
		initConnection();

	}

	public void initConnection() throws IOException {
		if (zkPrivateIPs == null) {
			LOGGER.error("HBase IP address not found\n");
			System.exit(-1);
		}
		if (hbaseMasterPort == null) {
			LOGGER.error("HBase master port not found\n");
			System.exit(-1);
		}
		if (!zkPrivateIPs.matches("\\d+.\\d+.\\d+.\\d+(,\\d+.\\d+.\\d+.\\d+)*")) {
			LOGGER.error("Malformed HBase IP address: " + zkPrivateIPs + "\n");
			System.exit(-1);
		}
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.master", zkPrivateIPs + ":" + hbaseMasterPort);
		conf.set("hbase.zookeeper.quorum", zkPrivateIPs);
		conf.set("hbase.zookeeper.property.clientport", "2181");
		connection = ConnectionFactory.createConnection(conf);
		contactsTable = connection.getTable(TableName.valueOf("contacts"));
		LOGGER.info("initConnection() OK");
	}

	/**
	 * serve query
	 *
	 * @param request  the request object that is passed to the servlet
	 * @param response the response object that the servlet uses to return the
	 *                 headers to the client
	 * @throws IOException      if an input or output error occurs
	 * @throws ServletException if the request for the HEAD could not be handled
	 */
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		// get input parameters
		if(request.getParameter("user_id") == null ||
		   request.getParameter("type") == null ||
		   request.getParameter("phrase") == null ||
		    request.getParameter("hashtag") == null
		){
			response.sendError(400, "request parameters are not complete!");
			return;
		}
		long uid = Long.parseLong(URLDecoder.decode(request.getParameter("user_id"), "UTF-8"));
		String type = URLDecoder.decode(request.getParameter("type"), "UTF-8");
		String phrase = URLDecoder.decode(request.getParameter("phrase"), "UTF-8");
		String hashtag = URLDecoder.decode(request.getParameter("hashtag"), "UTF-8");
		LOGGER.info("[query] uid = " + uid + ", phrase = " + phrase + ", hashtag = " + hashtag);

		// calculate scores
		ContactsResult contactsResult = queryHBase(uid);
		List<Score> scoreList = null;
		if(contactsResult == null) {
			scoreList = new ArrayList<>();
		} else {
			scoreList = Query2Helper.getScores(uid, contactsResult, type, phrase, hashtag);
		}

		// send response
		String responseText = Query2Helper.buildReponseText(scoreList);
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(responseText);
		writer.close();
	}

	private ContactsResult queryHBase(long uid) throws IOException {
		// Instantiating Get class
		Get get = new Get(Bytes.toBytes("" + uid));
		LOGGER.info("[hbase] get 'contacts', " + uid);
		Result result = contactsTable.get(get);
		if (result == null)
			return null;

		// Reading values from Result class object
		byte[] contactsByte = result.getValue(Bytes.toBytes("data"), Bytes.toBytes("contacts"));

		// Printing the values
		String contactsStr = Bytes.toString(contactsByte);

		ContactsResult contactsResult = gson.fromJson(contactsStr, ContactsResult.class);
		return contactsResult;
	}
}
