package edu.cmu.cc150.tweet;

import static io.undertow.servlet.Servlets.defaultContainer;
import static io.undertow.servlet.Servlets.deployment;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletInfo;

/**
 * You should NOT modify this file.
 */
public class Main {
	/**
	 * The constructor.
	 */
	public Main() {
	}

	/**
	 * Base path.
	 */
	public static final String PATH = "/";

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getRootLogger();

	/**
	 * The main entry of the website.
	 *
	 * @param args run args
	 */
	public static void main(String[] args) {
		LOGGER.setLevel(Level.OFF);
		if (args.length != 1 || (!"mysql".equals(args[0]) && !"hbase".equals(args[0]))) {
			LOGGER.error("please provide one args: mysql / hbase");
			return;
		}
		
		ServletInfo servletInfoQ1 = Servlets.servlet("Query1", Query1.class).addMapping("/q1");
		
		try {
			ServletInfo servletInfoQ2 = null;
			ServletInfo servletInfoQ3 = null;
			if ("mysql".equals(args[0])) {
				servletInfoQ2 = Servlets.servlet("MySQLServlet", MySQLServlet.class).addMapping("/q2");
				servletInfoQ3 = Servlets.servlet("MySQLQ3", MySQLQ3.class).addMapping("/q3");
				System.out.println("MySQL server starting");
			} else {
				servletInfoQ2 = Servlets.servlet("HBaseServlet", HBaseServlet.class).addMapping("/q2");
				servletInfoQ3 = Servlets.servlet("HBaseQ3", HBaseQ3.class).addMapping("/q3");
				System.out.println("HBase server starting");
			}

			DeploymentInfo servletBuilder = deployment().setClassLoader(Main.class.getClassLoader())
					.setContextPath(PATH).setDeploymentName("handler.war").addServlets(
						servletInfoQ1, servletInfoQ2, servletInfoQ3);

			DeploymentManager manager = defaultContainer().addDeployment(servletBuilder);
			manager.deploy();

			HttpHandler servletHandler = manager.start();
			PathHandler path = Handlers.path(Handlers.redirect(PATH)).addPrefixPath(PATH, servletHandler);

			Undertow server = Undertow.builder().addHttpListener(80, "0.0.0.0").setHandler(path).build();
			server.start();
		} catch (ServletException e) {
			throw new RuntimeException(e);
		}
	}
}
