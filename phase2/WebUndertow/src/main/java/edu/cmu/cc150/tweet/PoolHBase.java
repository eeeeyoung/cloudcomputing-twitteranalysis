package edu.cmu.cc150.tweet;

import java.io.IOException;
import org.apache.hadoop.hbase.client.Connection;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.ConnectionFactory;

public class PoolHBase {
	private List<Connection> pool = new ArrayList<>();
	private static int INITIAL_POOL_SIZE = 10;
	private int cur;

	public PoolHBase(String zkPrivateIPs, String hbaseMasterPort) {
		Configuration conf = HBaseConfiguration.create();
		conf.set("hbase.master", zkPrivateIPs + ":" + hbaseMasterPort);
		conf.set("hbase.zookeeper.quorum", zkPrivateIPs);
		conf.set("hbase.zookeeper.property.clientport", "2181");
		for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
			Connection connection = null;
			try {
				connection = ConnectionFactory.createConnection(conf);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(connection != null) {
				pool.add(connection);
			}
		}
		cur = 0;
	}

	public Connection getConnection() {
		Connection connection = null;
		synchronized (pool) {
			connection = pool.get(cur);
			cur = (cur + 1) % INITIAL_POOL_SIZE;
		}
		return connection;
	}
}