package edu.cmu.cc150.tweet;

public class TopicWord implements Comparable<TopicWord> {
    private String word;
    private Double score;

    public TopicWord(String word, Double score) {
        this.word = word;
        this.score = score;
    }

    @Override
    public int compareTo(TopicWord other) {
        if (this.score < other.score) {
            return 1;
        }
        if (this.score.equals(other.score)) {
            return this.word.compareTo(other.word);
        }
        return -1;
    }

    public String getWord() {
        return word;
    }
    
    public String getScore() {
        return String.format("%.2f", score);
    }
}
