package edu.cmu.cc150.tweet;

import java.util.*;

import org.apache.log4j.Logger;

import static java.util.stream.Collectors.toMap;

public class TopicWords {
	
	private static final Logger LOGGER = Logger.getRootLogger();

    public static HashMap<String, ArrayList> getTopicWords(HashMap<String, String> text, HashMap<String, Long> impactScore, Integer n1, Integer n2, HashMap<String, String> censored) {
        HashMap<String, HashSet<String>> InvertedIndex = new HashMap<>();
        HashMap<String, Double> cumScore = new HashMap<>();
        HashSet<String> topicRange = new HashSet<>();
        List<String> stopWordsList = Arrays.asList(stopWords);
        for (String tweetId : text.keySet()) {
            String[] words = text.get(tweetId).toLowerCase(Locale.ENGLISH).split("[^A-Za-z0-9'-]");  // words are lower case
            HashSet<String> uniqueWord = new HashSet<String>();
            for (String word : words) {
                if (!stopWordsList.contains(word) && word.replaceAll("[^A-Za-z]", "").length() != 0) {  // words in the StopWordList is not considered
                    uniqueWord.add(word);            // UniqueWord is a set
                }                                   // I am a ladder
            }
            updateMap(InvertedIndex, cumScore, uniqueWord, tweetId, words, impactScore);
        }
        for (String word : cumScore.keySet()) {
            double IDF = (double) text.size() / InvertedIndex.get(word).size();
            cumScore.put(word, cumScore.get(word) * Math.log(IDF));
        }

        // Sort Topic Words first by their score, then break ties by lexicographical order

        HashMap<String, ArrayList> outputMap = new HashMap<>();
        ArrayList <TopicWord> topicWordsOutput = new ArrayList<>();
        ArrayList <TopicWord> topicWords = new ArrayList<>();
        for (String key : cumScore.keySet()) {
            topicWordsOutput.add(new TopicWord(key, cumScore.get(key)));
        }
        Collections.sort(topicWordsOutput);
        // Get all valid tweets that contain at least one topic word
        if (n1 < topicWordsOutput.size()) {
            for (int i = 0; i < n1; i++) {
                for (String ele : InvertedIndex.get(topicWordsOutput.get(i).getWord()))
                    topicRange.add(ele);
                topicWords.add(topicWordsOutput.get(i));
            }
        } else {
            for (int i = 0; i < topicWordsOutput.size(); i++) {
                for (String ele : InvertedIndex.get(topicWordsOutput.get(i).getWord()))
                    topicRange.add(ele);
                topicWords.add(topicWordsOutput.get(i));
            }
        }

        ArrayList <Tweet> tweets = new ArrayList<>();
        ArrayList <Tweet> tweetsOutput = new ArrayList<>();
        for (String tweetId : censored.keySet()) {
            if (topicRange.contains(tweetId)) {
                tweets.add(new Tweet(tweetId, impactScore.get(tweetId)));
            }
        }
        Collections.sort(tweets);

        if (n2 < tweets.size()) {
            for (int i = 0; i < n2; i++) {
                tweetsOutput.add(tweets.get(i));
            }
        } else {
            for (int i = 0; i < tweets.size(); i++) {
                tweetsOutput.add(tweets.get(i));
            }
        }
        outputMap.put("TopicWords", topicWords);
        outputMap.put("Tweets", tweetsOutput);
        return outputMap;
    }

    private static final String[] stopWords = {"a", "about", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "amoungst", "amount", "an", "and", "another", "any", "anyhow", "anyone", "anything", "anyway", "anywhere", "are", "aren't", "around", "as", "at", "back", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom", "but", "by", "call", "can", "cannot", "cant", "can't", "co", "computer", "con", "could", "couldnt", "couldn't", "cry", "de", "describe", "detail", "didn't", "do", "doesn't", "done", "don't", "down", "due", "during", "each", "eg", "eight", "either", "eleven", "else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "hadn't", "has", "hasnt", "hasn't", "have", "haven't", "he", "he'd", "he'll", "hence", "her", "here", "hereafter", "hereby", "herein", "here's", "hereupon", "hers", "herself", "he's", "him", "himself", "his", "how", "however", "how's", "hundred", "i", "i'd", "ie", "if", "i'll", "i'm", "in", "inc", "indeed", "interest", "into", "is", "isn't", "it", "its", "it's", "itself", "i've", "keep", "last", "latter", "latterly", "least", "less", "let's", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "mustn't", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own", "part", "per", "perhaps", "please", "put", "rather", "re", "rt", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "that's", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "there's", "thereupon", "these", "they", "they'd", "they'll", "they're", "they've", "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "wasn't", "we", "we'd", "well", "we'll", "were", "we're", "weren't", "we've", "what", "whatever", "what's", "when", "whence", "whenever", "when's", "where", "whereafter", "whereas", "whereby", "wherein", "where's", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "who's", "whose", "why", "why's", "will", "with", "within", "without", "won't", "would", "wouldn't", "yet", "you", "you'd", "you'll", "your", "you're", "yours", "yourself", "yourselves", "you've"};

    private static void updateMap(HashMap<String, HashSet<String>> II, HashMap<String, Double> cumScore, HashSet<String> uniqueWord, String tweetId, String[] words, HashMap<String, Long> impactScore) {
        HashMap<String, Double> scores = getWCScore(uniqueWord, words);
        for (String word : uniqueWord) {
            II.putIfAbsent(word, new HashSet<String>());
            II.get(word).add(tweetId);
            cumScore.put(word, cumScore.getOrDefault(word, 0.0) + scores.get(word) * (Math.log(impactScore.get(tweetId) + 1)));
        }
    }

    private static HashMap<String, Double> getWCScore(HashSet<String> uniqueWord, String[] words) {
        HashMap<String, Double> scores = new HashMap<>();
        for (String ele : uniqueWord) {
            scores.put(ele, 0.0);
        }
        int size = 0;
        for (String a : words) {
            if (uniqueWord.contains(a)) {
                scores.put(a, scores.get(a) + 1.0);
            }
            if (a.replaceAll("[^A-Za-z]", "").length() != 0) {
                size += 1;
            }
        }
        for (String key : scores.keySet()) {
            scores.put(key, scores.get(key)/size);
        }
        return scores;
    }


}

