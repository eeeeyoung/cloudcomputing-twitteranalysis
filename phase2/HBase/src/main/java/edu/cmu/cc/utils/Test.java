package edu.cmu.cc.utils;

public class Test {
	public static void main(String[] args){
		long l = Long.MAX_VALUE;
		String str = String.format("%019d", l);
		System.out.println(str);
		long unixTime = 1555204228L;
		String unixTimeStr = String.format("%010d", unixTime);
		System.out.println(unixTimeStr);
		l = 0L;
		str = String.format("%019d", l);
		System.out.println(str);
		unixTime = 0L;
		unixTimeStr = String.format("%010d", unixTime);
		System.out.println(unixTimeStr);
	}
}
