# test data
put ’<table name>’,’row1’,’<colfamily:colname>’,’<value>’
put "tweets", "uid1", "data:1", "content1"
put "tweets", "uid1", "data:2", "content2"

scan "tweets", {FILTER =>"QualifierFilter(>=, 'binary:2') AND QualifierFilter(<=, 'binary:2') "}

# small data set
scan "tweets", {STARTROW => '0000000001021364672', ENDROW => '0000000001021385673', FILTER =>"QualifierFilter(>=, 'binary:1396105072') AND QualifierFilter(<=, 'binary:1401139459') "}

# import real data
create 'tweets', 'data'
count 'tweets', INTERVAL => 100000

# snapshot
hbase snapshot create -n snapshotTweets -t tweets
hbase snapshot create -n snapshotContacts -t contacts

hbase snapshot info -snapshot snapshotTweets
hbase snapshot info -snapshot snapshotContacts

hbase snapshot export -snapshot snapshotTweets -copy-to s3://emrbackup-cc150/hbase/snapshotTweets -mappers 8
hbase snapshot export -snapshot snapshotContacts -copy-to s3://emrbackup-cc150/hbase/snapshotContacts -mappers 8

sudo -u hbase hbase snapshot export \
-D hbase.rootdir=s3://emrbackup-cc150/hbase/snapshotTweets \
-snapshot snapshotTweets \
-copy-to hdfs://172.31.0.202:8020/user/hbase \
-mappers 2

sudo -u hbase hbase snapshot export \
-D hbase.rootdir=s3://emrbackup-cc150/hbase/snapshotContacts \
-snapshot snapshotContacts \
-copy-to hdfs://172.31.0.202:8020/user/hbase \
-mappers 1

