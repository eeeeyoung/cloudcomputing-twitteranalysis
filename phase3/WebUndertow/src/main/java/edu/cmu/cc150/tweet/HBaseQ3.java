package edu.cmu.cc150.tweet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Consistency;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import edu.cmu.cc150.tweet.query3.TweetsResult;

public class HBaseQ3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/** the private IP address(es) of HBase zookeeper nodes */
	private static String zkPrivateIPs = System.getenv("zkPrivateIPs");
	private static String hbaseMasterPort = System.getenv("hbaseMasterPort");
	private static Gson gson = new Gson();
	private static final Logger LOGGER = Logger.getRootLogger();
	private static final String TEAM_ID = "CC150CrackingTheCodingInterview";
	private static final String AWS_ID = "384893777434";
	private static PoolHBase pool;

	/**
	 * Initialize SQL connection.
	 *
	 * @throws ClassNotFoundException when an application fails to load a class
	 * @throws SQLException           on a database access error or other errors
	 * @throws IOException
	 */
	public HBaseQ3() throws ClassNotFoundException, SQLException, IOException {

		if (zkPrivateIPs == null) {
			LOGGER.error("HBase IP address not found\n");
			System.exit(-1);
		}
		if (hbaseMasterPort == null) {
			LOGGER.error("HBase master port not found\n");
			System.exit(-1);
		}
		if (!zkPrivateIPs.matches("\\d+.\\d+.\\d+.\\d+(,\\d+.\\d+.\\d+.\\d+)*")) {
			LOGGER.error("Malformed HBase IP address: " + zkPrivateIPs + "\n");
			System.exit(-1);
		}
		pool = new PoolHBase(zkPrivateIPs, hbaseMasterPort);
		LOGGER.info("HBase initConnection() OK");


	}

	/**
	 * serve query
	 *
	 * @param request  the request object that is passed to the servlet
	 * @param response the response object that the servlet uses to return the
	 *                 headers to the client
	 * @throws IOException      if an input or output error occurs
	 * @throws ServletException if the request for the HEAD could not be handled
	 */
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		LOGGER.warn("------------------------------------------------");
		long t1 = System.currentTimeMillis();
		LOGGER.warn(request.getRequestURL() + "?" + request.getQueryString());
		boolean isValid = true;
		String responseText = TEAM_ID + "," + AWS_ID;
		// get input parameters
		if (request.getParameter("uid_start") == null || request.getParameter("uid_end") == null
				|| request.getParameter("time_start") == null || request.getParameter("time_end") == null
				|| request.getParameter("n1") == null || request.getParameter("n2") == null) {
			response.sendError(400, "request parameters are not complete!");
			return;
		}
		long uidStart = -1;
		long uidEnd = -1;
		long timeStart = -1;
		long timeEnd = -1;
		int n1 = -1;
		int n2 = -1;
		try {
			uidStart = Long.parseLong(URLDecoder.decode(request.getParameter("uid_start"), "UTF-8"));
			uidEnd = Long.parseLong(URLDecoder.decode(request.getParameter("uid_end"), "UTF-8"));
			timeStart = Long.parseLong(URLDecoder.decode(request.getParameter("time_start"), "UTF-8"));
			timeEnd = Long.parseLong(URLDecoder.decode(request.getParameter("time_end"), "UTF-8"));
			n1 = Integer.parseInt(URLDecoder.decode(request.getParameter("n1"), "UTF-8"));
			n2 = Integer.parseInt(URLDecoder.decode(request.getParameter("n2"), "UTF-8"));
		} catch (Exception e) {
			// not validilty
			isValid = false;
		}

		if (uidStart >= 0 && uidEnd > 0 && timeStart >= 0 && timeEnd > 0 && n1 >= 0 && n2 >= 0) {
			if (uidEnd < uidStart || timeEnd < timeStart) {
				isValid = false;
			}
		} else {
			isValid = false;
		}
		LOGGER.info(
				"[hbase] do get, uid [" + uidStart + ", " + uidEnd + "], time = [" + timeStart + ", " + timeEnd + "]");

		if (!isValid) {
			response.setContentType("text/plain; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(responseText);
			writer.close();
			LOGGER.info("invalid request");
			return;
		}

		// calculate scores
		HashMap<String, String> censoredTextMap = new HashMap<String, String>();
		HashMap<String, Long> impactScoreMap = new HashMap<String, Long>();
		HashMap<String, String> noUrlTextMap = new HashMap<String, String>();

		long t2 = System.currentTimeMillis();
		LOGGER.warn("before query hbase " + (t2 - t1));
		try {
			queryHBase(uidStart, uidEnd, timeStart, timeEnd, censoredTextMap, impactScoreMap, noUrlTextMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		long t3 = System.currentTimeMillis();
		LOGGER.warn("query hbase " + (t3 - t2));
		HashMap<String, ArrayList> output = TopicWords.getTopicWords(noUrlTextMap, impactScoreMap, n1, n2,
				censoredTextMap);

		ArrayList<TopicWord> topicWords = output.get("TopicWords");
		ArrayList<Tweet> tweets = output.get("Tweets");

		int i = 0;
		for (TopicWord topicWord : topicWords) {
			if (i == 0) {
				responseText = responseText + "\n" + Censor.getCensored(topicWord.getWord()) + ":"
						+ topicWord.getScore();
				i = 1;
			} else {
				responseText = responseText + "\t" + Censor.getCensored(topicWord.getWord()) + ":"
						+ topicWord.getScore();
			}
		}

		for (Tweet twt : tweets) {
			responseText = responseText + "\n" + twt.getScore() + "\t" + twt.getTweetId() + "\t"
					+ censoredTextMap.get(twt.getTweetId());
		}

		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(responseText);
		writer.close();
		long t4 = System.currentTimeMillis();
		LOGGER.warn("after query sql " + (t4 - t3));
	}

	private void queryHBase(long uidStart, long uidEnd, long timeStart, long timeEnd,
			HashMap<String, String> censoredTextMap, HashMap<String, Long> impactScoreMap,
			HashMap<String, String> noUrlTextMap) throws IOException {
		LOGGER.info("queryHBase()");

		String uidStartStr = String.format("%019d", uidStart);
		String uidEndStr = String.format("%019d", uidEnd);
		String timeStartStr = String.format("%010d", timeStart);
		String timeEndStr = String.format("%010d", timeEnd);

		LOGGER.info("[hbase] scan 'tweets', uid [" + uidStartStr + ", " + uidEndStr + "], time = [" + timeStartStr
				+ ", " + timeEndStr + "]");

		List<Filter> filters = new ArrayList<Filter>(2);
		QualifierFilter filter1 = new QualifierFilter(CompareOp.GREATER_OR_EQUAL,
				new BinaryComparator(Bytes.toBytes(timeStartStr)));
		filters.add(filter1);

		QualifierFilter filter2 = new QualifierFilter(CompareOp.LESS_OR_EQUAL,
				new BinaryComparator(Bytes.toBytes(timeEndStr)));
		filters.add(filter2);

		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL, filters);

		Scan scan = new Scan().withStartRow(Bytes.toBytes(uidStartStr)).withStopRow(Bytes.toBytes(uidEndStr), true);
		scan.setFilter(filterList);
		// scan.setBatch(100);
		// scan.setCaching(100);
		scan.setConsistency(Consistency.TIMELINE);
		scan.setSmall(true);
		
		Connection connection = pool.getConnection();
		Table tweetsTable = connection.getTable(TableName.valueOf("tweets"));

		long t1 = System.currentTimeMillis();

		ResultScanner scanner = tweetsTable.getScanner(scan);

		long t2 = System.currentTimeMillis();
		LOGGER.warn("getScanner " + (t2 - t1));
		for (Result result : scanner) {
			while (result.advance()) {
				Cell cell = result.current();
				String content = Bytes.toString(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength());
				TweetsResult tweetsResult = gson.fromJson(content, TweetsResult.class);
				String tid = tweetsResult.tweet_id;
				censoredTextMap.put(tid, tweetsResult.censored_text);
				impactScoreMap.put(tid, tweetsResult.impactScore);
				noUrlTextMap.put(tid, tweetsResult.noURL);
			}
		}

		scanner.close();
	}
}
