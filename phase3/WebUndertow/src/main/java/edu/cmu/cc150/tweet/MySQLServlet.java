package edu.cmu.cc150.tweet;

import java.io.IOException;
import java.io.PrintWriter;

import java.net.URLDecoder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

import edu.cmu.cc150.tweet.query2.ContactsResult;
import edu.cmu.cc150.tweet.query2.Query2Helper;
import edu.cmu.cc150.tweet.query2.Score;

public class MySQLServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	/**
	 * JDBC driver of MySQL Connector/J.
	 */
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	/**
	 * Database name.
	 */
	private static final String DB_NAME = "twitter_db";

	private static final Logger LOGGER = Logger.getRootLogger();

	/**
	 * The endpoint of the database.
	 *
	 * To avoid hardcoding credentials, use environment variables to include the
	 * credentials.
	 *
	 * e.g., before running "mvn clean package exec:java" to start the server run
	 * the following commands to set the environment variables. export
	 * MYSQL_HOST=... export MYSQL_NAME=... export MYSQL_PWD=...
	 */
	private static final String MYSQL_HOST = System.getenv("MYSQL_HOST");
	/**
	 * MySQL username.
	 */
	private static final String MYSQL_NAME = System.getenv("MYSQL_NAME");
	/**
	 * MySQL Password.
	 */
	private static final String MYSQL_PWD = System.getenv("MYSQL_PWD");

	/**
	 * MySQL URL.
	 */
	private static final String URL = "jdbc:mysql://" + MYSQL_HOST + ":3306/" + DB_NAME + "?useSSL=false";

	private Pool pool;
	private static Gson gson = new Gson();

	/**
	 * Initialize SQL connection.
	 *
	 * @throws ClassNotFoundException when an application fails to load a class
	 * @throws SQLException           on a database access error or other errors
	 */
	public MySQLServlet() throws ClassNotFoundException, SQLException {
		Class.forName(JDBC_DRIVER);
		Objects.requireNonNull(MYSQL_HOST);
		Objects.requireNonNull(MYSQL_NAME);
		Objects.requireNonNull(MYSQL_PWD);
		this.pool = new Pool(URL, MYSQL_NAME, MYSQL_PWD);
	}

	private ContactsResult queryMySQL(long uid) throws IOException, SQLException, InterruptedException {
		// Instantiating Get class
		LOGGER.info("[mysql] get 'contacts', " + uid);
		ContactsResult contactsResult = null;
		PreparedStatement ctPstmt = this.pool.getCtPs();
		try{
			ctPstmt.setString(1, uid + "");
			ResultSet ctRs = ctPstmt.executeQuery();
			if (!ctRs.next()) {
				this.pool.releaseCtPs(ctPstmt);
				return null;
			}

			// Printing the values
			String dataStr = ctRs.getString("data");
			contactsResult = gson.fromJson(dataStr, ContactsResult.class);
		}  catch (Exception e) {
			e.printStackTrace();
		}
		this.pool.releaseCtPs(ctPstmt);
		
		return contactsResult;
	}

	/**
	 * serve query
	 *
	 * @param request  the request object that is passed to the servlet
	 * @param response the response object that the servlet uses to return the
	 *                 headers to the client
	 * @throws IOException      if an input or output error occurs
	 * @throws ServletException if the request for the HEAD could not be handled
	 */
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		// get input parameters
		if (request.getParameter("user_id") == null || request.getParameter("type") == null
				|| request.getParameter("phrase") == null || request.getParameter("hashtag") == null) {
			response.sendError(400, "request parameters are not complete!");
			return;
		}
		long uid = Long.parseLong(URLDecoder.decode(request.getParameter("user_id"), "UTF-8"));
		String type = URLDecoder.decode(request.getParameter("type"), "UTF-8");
		String phrase = URLDecoder.decode(request.getParameter("phrase"), "UTF-8");
		String hashtag = URLDecoder.decode(request.getParameter("hashtag"), "UTF-8");	
		
		// calculate scores
		ContactsResult contactsResult = null;
		try {
			contactsResult = queryMySQL(uid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		List<Score> scoreList = null;
		if (contactsResult == null) {
			scoreList = new ArrayList<>();
		} else {
			scoreList = Query2Helper.getScores(uid, contactsResult, type, phrase, hashtag);
		}

		// send response
		String responseText = Query2Helper.buildReponseText(scoreList);
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(responseText);
		writer.close();
	}
}
