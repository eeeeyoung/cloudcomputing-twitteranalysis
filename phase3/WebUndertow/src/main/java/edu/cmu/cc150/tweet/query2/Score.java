package edu.cmu.cc150.tweet.query2;

public class Score implements Comparable<Score> {
	public String user;
	public double score;
	public String lastTweet;
	public String screenName;
	public String description;

	public Score(String user, double score) {
		this.user = user;
		this.score = score;
	}

	@Override
	public int compareTo(Score other) {
		if (other.score > this.score) {
			return 1;
		}
		if (other.score == this.score) {
			Long thisId = Long.parseLong(this.user);
			Long otherId = Long.parseLong(other.user);
			if (thisId > otherId)
				return -1;
			else if (thisId < otherId)
				return 1;
			else
				return 0;
		}
		return -1;
	}

}
