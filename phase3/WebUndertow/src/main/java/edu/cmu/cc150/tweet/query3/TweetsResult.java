package edu.cmu.cc150.tweet.query3;

import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class TweetsResult {
	public String censored_text;
	public String tweet_id;
	public String user_id;
	public String created_at;
	public long impactScore;
	public String noURL;
}
