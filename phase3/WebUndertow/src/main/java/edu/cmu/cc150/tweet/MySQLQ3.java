package edu.cmu.cc150.tweet;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.log4j.Logger;

import com.google.gson.Gson;

import edu.cmu.cc150.tweet.query3.TweetsResult;

public class MySQLQ3 extends HttpServlet {

	private static final long serialVersionUID = 1L;
	/**
	 * JDBC driver of MySQL Connector/J.
	 */
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	/**
	 * Database name.
	 */
	private static final String DB_NAME = "twitter_db";

	private static final Logger LOGGER = Logger.getRootLogger();
	
	private static final String TEAM_ID = "CC150CrackingTheCodingInterview";
	private static final String AWS_ID = "384893777434";

	/**
	 * The endpoint of the database.
	 *
	 * To avoid hardcoding credentials, use environment variables to include the
	 * credentials.
	 *
	 * e.g., before running "mvn clean package exec:java" to start the server run
	 * the following commands to set the environment variables. export
	 * MYSQL_HOST=... export MYSQL_NAME=... export MYSQL_PWD=...
	 */
	private static final String MYSQL_HOST = System.getenv("MYSQL_HOST");
	/**
	 * MySQL username.
	 */
	private static final String MYSQL_NAME = System.getenv("MYSQL_NAME");
	/**
	 * MySQL Password.
	 */
	private static final String MYSQL_PWD = System.getenv("MYSQL_PWD");

	/**
	 * MySQL URL.
	 */
	private static final String URL = "jdbc:mysql://" + MYSQL_HOST + ":3306/" + DB_NAME + "?useSSL=false";

	private PoolQ3 pool;
	private static Gson gson = new Gson();

	/**
	 * Initialize SQL connection.
	 *
	 * @throws ClassNotFoundException when an application fails to load a class
	 * @throws SQLException           on a database access error or other errors
	 */
	public MySQLQ3() throws ClassNotFoundException, SQLException {
		Class.forName(JDBC_DRIVER);
		Objects.requireNonNull(MYSQL_HOST);
		Objects.requireNonNull(MYSQL_NAME);
		Objects.requireNonNull(MYSQL_PWD);
		this.pool = new PoolQ3(URL, MYSQL_NAME, MYSQL_PWD);
	}

	private void queryMySQL(long uidStart, long uidEnd, long timeStart, long timeEnd,
			HashMap<String, String> censoredTextMap, HashMap<String, Long> impactScoreMap,
			HashMap<String, String> noUrlTextMap) throws IOException, SQLException, InterruptedException {
		LOGGER.info("[mysql] get 'contacts', " + uidStart);
		PreparedStatement ctPstmt = this.pool.getCtPs();
		try {
			ctPstmt.setLong(1, uidStart);
			ctPstmt.setLong(2, uidEnd);
			ctPstmt.setLong(3, timeStart);
			ctPstmt.setLong(4, timeEnd);
		
			ResultSet ctRs = ctPstmt.executeQuery();
		

			while(ctRs.next()) {
				// Printing the values
				String dataStr = ctRs.getString("data");
				TweetsResult tweetsResult = gson.fromJson(dataStr, TweetsResult.class);	
				String tid = tweetsResult.tweet_id;
				censoredTextMap.put(tid, tweetsResult.censored_text);
				impactScoreMap.put(tid, tweetsResult.impactScore);
				noUrlTextMap.put(tid, tweetsResult.noURL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		this.pool.releaseCtPs(ctPstmt);
	}

	/**
	 * serve query
	 *
	 * @param request  the request object that is passed to the servlet
	 * @param response the response object that the servlet uses to return the
	 *                 headers to the client
	 * @throws IOException      if an input or output error occurs
	 * @throws ServletException if the request for the HEAD could not be handled
	 */
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		boolean isValid = true;
		String responseText = TEAM_ID + "," + AWS_ID;
		// get input parameters
		if (request.getParameter("uid_start") == null || request.getParameter("uid_end") == null
				|| request.getParameter("time_start") == null || request.getParameter("time_end") == null
				|| request.getParameter("n1") == null || request.getParameter("n2") == null) {
			response.sendError(400, "request parameters are not complete!");
			return;
		}
		long uidStart = -1;
		long uidEnd = -1;
		long timeStart = -1;
		long timeEnd = -1;
		int n1 = -1;
		int n2 = -1;
		try {
			uidStart = Long.parseLong(URLDecoder.decode(request.getParameter("uid_start"), "UTF-8"));
			uidEnd = Long.parseLong(URLDecoder.decode(request.getParameter("uid_end"), "UTF-8"));
			timeStart = Long.parseLong(URLDecoder.decode(request.getParameter("time_start"), "UTF-8"));
			timeEnd = Long.parseLong(URLDecoder.decode(request.getParameter("time_end"), "UTF-8"));
			n1 = Integer.parseInt(URLDecoder.decode(request.getParameter("n1"), "UTF-8"));
			n2 = Integer.parseInt(URLDecoder.decode(request.getParameter("n2"), "UTF-8"));
		} catch (Exception e) {
			// not validilty 
			isValid = false;
		}
		
		if (uidStart >= 0 && uidEnd > 0 && timeStart >= 0 && timeEnd > 0 && n1 >= 0 && n2 >= 0) {
			if (uidEnd < uidStart || timeEnd < timeStart) {
				isValid = false;
			}	
		} else {
			isValid = false;
		}

		
		if (!isValid) {
			response.setContentType("text/plain; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(responseText);
			writer.close();
			return;
		}
			
		// calculate scores
		HashMap<String, String> censoredTextMap = new HashMap<String, String>();
		HashMap<String, Long> impactScoreMap = new HashMap<String, Long>();
		HashMap<String, String> noUrlTextMap = new HashMap<String, String>();
		try {
			queryMySQL(uidStart, uidEnd, timeStart, timeEnd, censoredTextMap, impactScoreMap, noUrlTextMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		HashMap<String, ArrayList> output = TopicWords.getTopicWords(noUrlTextMap, impactScoreMap, n1, n2, censoredTextMap);
		
		ArrayList <TopicWord> topicWords = output.get("TopicWords");
		ArrayList <Tweet> tweets = output.get("Tweets");
		
		int i = 0;
		for (TopicWord topicWord:topicWords) {
			if (i == 0) {
				responseText = responseText + "\n" + Censor.getCensored(topicWord.getWord()) + ":" + topicWord.getScore();
				i = 1;
			} else {
				responseText = responseText + "\t" + Censor.getCensored(topicWord.getWord()) + ":" + topicWord.getScore();
			}	
		}
		
		for (Tweet twt:tweets) {
			responseText = responseText + "\n" + twt.getScore() + "\t" + twt.getTweetId() + "\t" + censoredTextMap.get(twt.getTweetId());
		}
		
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(responseText);
		writer.close();
	}
}
