package edu.cmu.cc150.tweet.query2;

public class Interaction {
	public long contactId;
	public long cntReply;
	public long cntRetweet;
	
	public String lastRetweet;
	public String lastReply;
	public String lastBoth;

	public String[] allReply;
	public String[] allRetweet;
	public String[] allBoth;

	public String[] replyHash;
	public String[] retweetHash;
	public String[] bothHash;
	
	public UserInfo userInfo;
	public double hashtagScore;
}
