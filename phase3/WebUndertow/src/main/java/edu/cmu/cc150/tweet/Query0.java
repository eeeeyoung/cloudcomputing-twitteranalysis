package edu.cmu.cc150.tweet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

public class Query0 extends HttpServlet {

	private static final long serialVersionUID = 1L;


	/**
	 * Initialize SQL conWebUndertowWebUndertownection.
	 * @return 
	 *
	 * @throws ClassNotFoundException when an application fails to load a class
	 * @throws SQLException           on a database access error or other errors
	 */
	public Query0() throws ClassNotFoundException, SQLException {
		
	}

	/**
	 * serve query
	 *
	 * @param request  the request object that is passed to the servlet
	 * @param response the response object that the servlet uses to return the
	 *                 headers to the client
	 * @throws IOException      if an input or output error occurs
	 * @throws ServletException if the request for the HEAD could not be handled
	 */
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {
		String result = "health check ok!";
		
		// send response
		response.setContentType("text/plain; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(result);
		writer.close();
	}
}
