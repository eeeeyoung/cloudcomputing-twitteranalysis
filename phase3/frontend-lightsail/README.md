## env
ssh -i /Users/mzhang/git-clones/CloudComputing/LightsailDefaultKey-us-east-1.pem ubuntu@3.94.186.163
sudo apt-get update
sudo apt-get install openjdk-8-jdk
sudo apt install maven
git clone https://github.com/CloudComputingTeamProject/CC150CrackingTheCodingInterview-S19.git

export MYSQL_HOST=readers3.cluster-custom-cdafbngatlum.us-east-1.rds.amazonaws.com


## start web
cd /home/ubuntu/CC150CrackingTheCodingInterview-S19/phase3/WebUndertow
sudo su
export MYSQL_HOST=reader3.cdafbngatlum.us-east-1.rds.amazonaws.com
export MYSQL_NAME=root
export MYSQL_PWD=12345678
mvn clean package
mvn exec:java -Dexec.args="mysql"

## sample query
http://3.94.186.163/q1?cc=eJyVktFOwzAMRX9lyjMPdmLHCb-CEOralBVt7UQLQ5r677jbVFJQkZan6DqRT058NuWuaFrzuHk6m2K_fxm-rvv3VH7qjiECMkr0Eh82pjgMl_C2NBmaQ9LIIDOIRT0nEp23DshoeVf0u6lcMSS23pvxWdOm0gyyMtgQ4zbE6cqxO03RseuHY9cmMz5slmh9aqu_aDdgCgRMji16nIHR-kAhktekTumSUAir-BGFFvhc2C1VglPWN69TH--CthJxjBfCW3-0gRAhRBVwhzGypNW8pURLJXPKjGFurC4TY4mZsbr7aIfpM9eM_Wb7TxkH4cgRZ2MMtCqMSIAW9Fty4qSof4RpX4oCQF5syI05QusYdGTcHb6EUPxiwgQ4CeW-bO7Lece1SObrLRVtfz3eptNV1kwV3DQH-qwQZcYiB56sfvoqVtDXgRnHbykf0R8=

http://3.94.186.163/q3?time_start=1398464619&time_end=1398571534&uid_start=741860618&uid_end=741864386&n1=3&n2=43