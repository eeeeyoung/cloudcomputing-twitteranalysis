# terraform init      Initialize a Terraform working directory
# terraform validate  Validates the Terraform files
# terraform fmt       Rewrites config files to canonical format
# terraform plan      Generate and show an execution plan
# terraform apply     Builds or changes infrastructure
# terraform destroy   Destroy Terraform-managed infrastructure

provider "aws" {
  region = "us-east-1"
}

resource "aws_lightsail_instance" "lightsail-instance" {
  name              = "frontend"
  availability_zone = "us-east-1b"
  blueprint_id      = "ubuntu_16_04_2"
  bundle_id         = "medium_"
  key_pair_name     = "${var.key_name}"
}