package edu.cmu.scs.cc.teamproject;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import java.util.ArrayList;
import java.util.StringJoiner;
import org.json.*;

public class TwitterMapper extends Mapper<Object, Text, Text, Text> {
    @Override
    public void map(Object key, Text value, Context context) {

        // Convert the input line to a JSON Object
        String valueString = value.toString();
        try {
            JSONObject tweetObject = new JSONObject(valueString);
            if (DataFilter.checkAllRules(tweetObject)) {
                String tweetId = DataFilter.getTweeterObjId(tweetObject);

                // Constructs the Tweet Output string
                StringJoiner joiner1 = new StringJoiner("\t");
                StringJoiner joiner2 = new StringJoiner("\t");

                String userId = DataFilter.getUserObjId(tweetObject);
                String[] TweetType = DataFilter.getTweetTypeAndID(tweetObject);
                // Gets "Reply", "Retweet", or "None"
                // Gets the Reply Id, Retweet Id, or "None"
                String tweetContent = DataFilter.getText(tweetObject).replaceAll("\r\n|\n|\r", "/n");

                // Get censoredTags, return "" if no hashtag found
                ArrayList censoredTags = DataFilter.getCensoredtag(tweetObject);  // Get censored hashtag for contacts
                // Get uncensoredTags
                ArrayList uncensoredTags = DataFilter.getHashtag(tweetObject);
                // Constructs the User Output string
                joiner1.add(userId);
                joiner1.add(DataFilter.getScreenname(tweetObject).replaceAll("\r\n|\n|\r", "/n").replaceAll("\t", "teamcc150"));
                joiner1.add(DataFilter.getDescription(tweetObject).replaceAll("\r\n|\n|\r", "/n").replaceAll("\t", "teamcc150"));
                if (!censoredTags.isEmpty()) {
                    joiner1.add(String.join(",", censoredTags));
                } else {
                    joiner1.add("");
                }
                String CreatedAt = DataFilter.getUnformattedDate(DataFilter.getCreated(tweetObject));
                joiner1.add(CreatedAt);
                joiner1.add(tweetId);
                context.write(new Text("user"+userId), new Text(joiner1.toString()));

                if (TweetType[0].equals("Retweet")) {
                    JSONObject retweeted_status = DataFilter.getRetweetedStatus(tweetObject);
                    joiner2.add(DataFilter.getUserObjId(retweeted_status));
                    joiner2.add(DataFilter.getScreenname(retweeted_status).replaceAll("\r\n|\n|\r", "/n").replaceAll("\t", "teamcc150"));
                    joiner2.add(DataFilter.getDescription(retweeted_status).replaceAll("\r\n|\n|\r", "/n").replaceAll("\t", "teamcc150"));
                    joiner2.add(""); // For empty hashtag
                    joiner2.add(CreatedAt);
                    joiner2.add(tweetId);
                    context.write(new Text("user"+DataFilter.getUserObjId(retweeted_status)), new Text(joiner2.toString()));
                }
                // Constructs the User interaction
                String peerId = TweetType[1];
                mapInteraction(context, userId, peerId, tweetId, TweetType[0], tweetContent, CreatedAt, uncensoredTags);
                if(!userId.equals(peerId)) {
                	mapInteraction(context, peerId, userId, tweetId, TweetType[0], tweetContent, CreatedAt, uncensoredTags);
                }
            }
        } catch (Exception e) {
            // Ignores Exception, JSON conversion error
        	e.printStackTrace();
        }
    }
	// key = i + userId
	// value = peerId \t tweetId \t retweetCnt \t replyCnt
    private void mapInteraction(Context context, String userId, String peerId, String tweetId, String tweetType, String tweetContent, String created_at, ArrayList uncensored)
		throws IOException, InterruptedException {

    	StringJoiner joiner = new StringJoiner("\t");
		joiner.add(peerId);
		joiner.add(tweetId);
		if("Retweet".equals(tweetType)) {
			joiner.add("1");
			joiner.add("0");
		}else if("Reply".equals(tweetType)) {
			joiner.add("0");
			joiner.add("1");
		}else {
			// skip for simple post which is not retweet or reply
		}
		joiner.add(tweetType);
		joiner.add(tweetContent);
        if (!uncensored.isEmpty()) {
            joiner.add(String.join(",", uncensored));
        } else {
            joiner.add(" ");
        }
		joiner.add(created_at);
        context.write(new Text("i"+userId), new Text(joiner.toString()));
	}
}
