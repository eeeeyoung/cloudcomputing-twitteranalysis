package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.json.JSONObject;
import java.util.*;

public class TwitterReducer extends Reducer<Text, Text, Text, Text> {

	private MultipleOutputs mOutput;

	@Override
	public void setup(Context context) {
		mOutput = new MultipleOutputs(context);
	}

	@Override
	public void cleanup(Context context) throws IOException, InterruptedException {
		mOutput.close();
	}

	private static String getLatestEntry(Map<Long, String> tM) {
		try {
			for (Map.Entry<Long, String> entry : tM.entrySet()) {
				String value = entry.getValue();
				if (value.equals("")) {
					return "";
				} else {
					return value;
				}
			}
		} catch (NullPointerException e) {
			// Map not initialized, therefore not exists
			return "";
		}
		return "";
	}
	/**
	 *
	 * @param tM the TreeMap
	 * @return a concatenated string containing every tweet separated by "teamcc150"
	 */
	private static String getAllEntry(Map<Long, String> tM) {
		try {
			StringJoiner all = new StringJoiner("teacmcc150");
			for (Map.Entry<Long, String> entry : tM.entrySet()) {
				String value = entry.getValue();
				if (value.equals("")) {
					// Do nothing
				} else {
					all.add(value);
				}
			}
			return all.toString();
		} catch (NullPointerException e) {
			// Map not initialized, therefore not exists
			return "";
		}
	}

	private static Integer getCounts(ArrayList<String> AL, String targetHashtag) {
		Integer count = 0;
		for (String ele : AL) {
			if (ele.equals(targetHashtag)) {
				count += 1;
			}
		}
		return count;
	}

	/**
	 *
	 * @param key
	 * @param values
	 * @param con
	 */
	@Override
	public void reduce(Text key, Iterable<Text> values, Context con) {
		Map<String, Integer> countsRetweet = new HashMap<>();
		Map<String, Integer> countsReply = new HashMap<>();
		Set<String> visitedTweets = new HashSet<>();

		// Yang
		ArrayList<Long> duplicateTimeStamp = new ArrayList<>();
		Map<Long, String> screenNames = new TreeMap<Long, String>(Collections.reverseOrder());
		Map<Long, String> descriptions = new TreeMap<Long, String>(Collections.reverseOrder());
		Map<String, TreeMap<Long, String>> peerReplyMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerRetweetMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerBothMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerReplyHash = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerRetweetHash = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerBothHash = new HashMap<String, TreeMap<Long, String>>();

		ArrayList<String> totalHashtag = new ArrayList<String>();
		boolean duplicateTweet = false;
		String userId = key.toString().substring(1);
		try {
			for (Text value : values) {
				if (value.toString().trim().isEmpty())
					continue;
				if (key.toString().charAt(0) == 't') {
					if (!duplicateTweet) {
						// Deprecated
						duplicateTweet = true;
					}
				} else {
					if (key.toString().charAt(0) == 'u') {
						String[] secondList = value.toString().split("\t");
						if (secondList.length != 6) {
							// Do nothing, malformed tweet
							System.out.println("Malformed");
						} else {
							Long timeStamp = Long.parseLong(secondList[secondList.length - 2]);
							Long tweetId = Long.parseLong(secondList[secondList.length -1]);
							if (!duplicateTimeStamp.contains(tweetId)) {
								duplicateTimeStamp.add(tweetId);
								screenNames.put(timeStamp, secondList[1]);
								descriptions.put(timeStamp, secondList[2]);
								// Testing Code
								for (String tag : secondList[3].split(",")) {
									if (!tag.equals("")) {
										totalHashtag.add(tag);
									}
								}
							}
						}
					} else if (key.toString().charAt(0) == 'i') {
						// input for counting interaction
						// key = i + userId
						// value = peerId \t tweetId \t retweetCnt \t replyCnt
						String[] items = value.toString().split("\t");
						if (items.length != 8) {
							continue;
						}
						String peerId = items[0];
						String tweetId = items[1];
						int retweetCnt = "1".equals(items[2]) ? 1 : 0;
						int replyCnt = "1".equals(items[3]) ? 1 : 0;
						if (!visitedTweets.contains(tweetId)) {
							countsRetweet.put(peerId, countsRetweet.getOrDefault(peerId, 0) + retweetCnt);
							countsReply.put(peerId, countsReply.getOrDefault(peerId, 0) + replyCnt);
							visitedTweets.add(tweetId);
							if (items[4].equals("Retweet")) {
								if (!peerRetweetMap.containsKey(items[0])) { // if peer id exists
									peerRetweetMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerBothMap.containsKey(items[0])) {
									peerBothMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerRetweetHash.containsKey(items[0])) {
									peerRetweetHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerBothHash.containsKey(items[0])){
									peerBothHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								}
								peerRetweetMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
								peerBothMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
								peerRetweetHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
								peerBothHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
							} else if (items[4].equals("Reply"))
							{
								if (!peerReplyMap.containsKey(items[0])) {
									peerReplyMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerBothMap.containsKey(items[0])) {
									peerBothMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerReplyHash.containsKey(items[0])) {
									peerReplyHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								if (!peerBothHash.containsKey(items[0])) {
									peerBothHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
								} else {
									assert true;
								}
								peerReplyMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
								peerBothMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
								peerReplyHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
								peerBothHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
							} else {
								// TweetType = "None" just ignore it
							}
						}
					}
				}
			}
			// yang output, constructing json object
			if (key.toString().charAt(0) == 'u') {
				StringJoiner sj = new StringJoiner("\t");
				sj.add(key.toString().substring(4));
				sj.add(getLatestEntry(screenNames));
				sj.add(getLatestEntry(descriptions));
				StringJoiner hashsj = new StringJoiner(" ");
				if (totalHashtag.size() == 0) {
					sj.add("");
				} else {
					Set<String> set = new HashSet<String>(totalHashtag);
					for (String hashtag : set) {
						if (!hashtag.trim().equals("")) {
							hashsj.add(hashtag);
							hashsj.add(getCounts(totalHashtag, hashtag).toString());
						}
					}
					sj.add(hashsj.toString());
				}
				mOutput.write("user", NullWritable.get(), new Text(sj.toString()));
			}
			// output for counting interaction
			// key = userId
			// value = peerId \t  retweetCnt \t replyCnt
			JSONObject outJson = new JSONObject();
//			for (String peerId : countsRetweet.keySet()) {
//				int retweetCnt = countsRetweet.get(peerId);
//				int replyCnt = countsReply.get(peerId);
//				// for mysql
//				StringJoiner joiner = new StringJoiner("\t");
//				joiner.add(userId);  // 1
//				joiner.add(peerId);  // 2
//				joiner.add("" + retweetCnt);  // 3
//				joiner.add("" + replyCnt);  // 4
//				joiner.add(getLatestEntry(peerReplyMap.get(peerId)));  // 5
//				joiner.add(getLatestEntry(peerRetweetMap.get(peerId)));  // 6
//				joiner.add(getLatestEntry(peerBothMap.get(peerId)));  // 7
//				joiner.add(getAllEntry(peerReplyMap.get(peerId)));  //8
//				joiner.add(getAllEntry(peerRetweetMap.get(peerId)));  // 9
//				joiner.add(getAllEntry(peerBothMap.get(peerId)));  //10
//				joiner.add(getAllEntry(peerReplyHash.get(peerId)));  // 11
//				joiner.add(getAllEntry(peerRetweetHash.get(peerId)));  // 12
//				joiner.add(getAllEntry(peerBothHash.get(peerId)));
				// mOutput.write("interaction", NullWritable.get(), new Text(joiner.toString()));

				// for hbase
				// StringJoiner joinerHbase = new StringJoiner("\t");
				// joinerHbase.add(userId + "-" + peerId);
				// joinerHbase.add("" + retweetCnt);
				// joinerHbase.add("" + replyCnt);
				// joiner.add(getLatestEntry(peerReplyMap.get(peerId)));
				// joiner.add(getLatestEntry(peerRetweetMap.get(peerId)));
				// joiner.add(getLatestEntry(peerBothMap.get(peerId)));
				// mOutput.write("interactionHbase", NullWritable.get(), new Text(joinerHbase.toString()));
			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
