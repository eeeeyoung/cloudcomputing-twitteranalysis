package edu.cmu.scs.cc.teamproject;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.json.*;

public class TwitterMapper extends Mapper<Object, Text, Text, Text> {
    @Override
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        String[] postSplit = value.toString().split("\t");
        String userId = postSplit[0];
        // interaction table, just remove the prefix "i"
        if (userId.startsWith("i")) {
            context.write(new Text(userId.substring(1)), new Text(postSplit[1]));
        } else {
            // user table
            JSONObject userObject = new JSONObject(postSplit[1]);
            JSONArray related = userObject.getJSONArray("related");
            userObject.remove("related");
            writeUser(userObject, userId.substring(1), "self", context);
            for (int i = 0; i < related.length(); i++) {
                writeUser(userObject, related.get(i).toString(), userId.substring(1), context);
            }
        }
    }
    /**
     *
     * @param userobject userObject passed by the outer function
     * @param u Output key of the mapper
     * @param c Inner identity of the user table
     * @param context Context context passed by the map function
     * @throws IOException
     * @throws InterruptedException
     */
    private void writeUser(JSONObject userobject, String u, String c, Context context) throws IOException, InterruptedException {
        userobject.put("who", c);
        context.write(new Text(u), new Text(userobject.toString()));
        userobject.remove("who");
    }
}