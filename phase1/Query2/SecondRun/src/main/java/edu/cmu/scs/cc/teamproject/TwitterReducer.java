package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.*;

public class TwitterReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		Iterator itr = values.iterator();
		List<JSONObject> aL = new ArrayList<JSONObject>();
		JSONObject reducer2output = new JSONObject();
		while (itr.hasNext()) {
			String a = itr.next().toString();
			if (a.startsWith("[")) {
				JSONArray interactions = new JSONArray(a);
				reducer2output.put("interactions", interactions);
			} else {
				// user info
				aL.add(new JSONObject(a));
			}
		}
		// First pass
		for (int i=0; i<aL.size(); i++) {
			if (aL.get(i).get("who").toString().equals("self")) {
				reducer2output.put("userInfo", aL.get(i));
				String senderHashtag = aL.get(i).get("censoredHashtags").toString();
				// Construct the senderHashmap
				String[] senderHashtags = senderHashtag.split(" ");
				Map userMap = getHashtagMap(senderHashtags);
				aL.remove(i);
				// Second pass, retrieve user entries with "who" != "self"
				// calculate score, put into reducer2output object
				for (int j = 0; j < aL.size(); j++) {
					String peerId = aL.get(j).get("who").toString();
					if (!peerId.equals("self")) {
						for (int k = 0; k < reducer2output.getJSONArray("interactions").length(); k++) {
							if (reducer2output.getJSONArray("interactions").getJSONObject(k).get("contactId").toString().equals(peerId)) {
								JSONObject peerObject = reducer2output.getJSONArray("interactions").getJSONObject(k);
								peerObject.put("userInfo", aL.get(j));
								// calculate score!!
								String[] peerHashtags = aL.get(j).get("censoredHashtags").toString().split(" ");
								Double peerScore = computeScore(userMap, peerHashtags);
								peerObject.put("hashtagScore", peerScore);
								// remove and add the new peerObjeact
								reducer2output.getJSONArray("interactions").remove(k);
								reducer2output.getJSONArray("interactions").put(peerObject);
							}
						}
					}
				}
				context.write(new Text(key.toString()), new Text(reducer2output.toString()));
				break;
			} else {
				// Do nothing
			}
		}
	}
	private Map<String, Integer> getHashtagMap(String[] inputString) {
		Map<String, Integer> map = new HashMap<>();
		if (inputString.length > 1) {
			for (int hash = 0; hash < inputString.length; hash = hash + 2) {
				try {
					map.put(inputString[hash].toLowerCase(Locale.ENGLISH), Integer.parseInt(inputString[hash + 1]));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}

	private Double computeScore(Map<String, Integer> userMap, String[] hashcount) {
		for (int i = 0; i < hashcount.length; i = i + 2) {
			int hashCounter = 0;
			if (userMap.containsKey(hashcount[i].toLowerCase(Locale.ENGLISH))) {
				try {
					hashCounter = hashCounter + userMap.get(hashcount[i].toLowerCase(Locale.ENGLISH)) + Integer.parseInt(hashcount[i + 1]);
					if (hashCounter > 10) {
						return (1 + Math.log(1 + hashCounter -10));
					} else {
						return 1.0;
					}
				} catch (Exception e) {
					e.printStackTrace();
					return 1.0;
				}
			}
		}
		return 1.0;
	}
}