package edu.cmu.cc150.tweet.query2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class Query2Helper {
	/** response headers */
	private static final String TEAM_ID = "CC150CrackingTheCodingInterview";
	private static final String AWS_ID = "384893777434";

	private static final Logger LOGGER = Logger.getRootLogger();

	/** return sorted scores from highest to lowest */
	public static ArrayList<Score> getScores(long uid, ContactsResult contactsResult, String type, String phrase,
			String hashtag) throws IOException {
		ArrayList<Score> scoreList = new ArrayList<Score>();

		Interaction[] interactions = contactsResult.interactions;
		if (interactions != null) {
			for (Interaction interaction : interactions) {
				long contactId = interaction.contactId;

				double interactionScore = getInteractionScore(interaction);
				double keywordsScore = getKeywordsScore(interaction, type, phrase, hashtag);
				double hashtagScore = interaction.hashtagScore;
				double totalScore = interactionScore * keywordsScore * hashtagScore;

				if (totalScore == 0) {
					continue;
				}

				Score score = new Score(contactId + "", totalScore);
				score.lastTweet = getLastTweet(type, interaction);
				score.description = interaction.userInfo.latestDesc;
				score.screenName = interaction.userInfo.latestName;
				scoreList.add(score);
			}
		}

		Collections.sort(scoreList);
		return scoreList;
	}

	public static String buildReponseText(List<Score> scoreList) {
		StringBuilder sb = new StringBuilder();
		sb.append(TEAM_ID + "," + AWS_ID);
		for (int i = 0; i < scoreList.size(); i++) {
			Score score = scoreList.get(i);
			sb.append("\n" + score.user + "\t" + score.screenName + "\t" + score.description + "\t" + score.lastTweet);
		}
		return sb.toString();
	}

	private static double getInteractionScore(Interaction contact) {
		long cntReply = contact.cntReply;
		long cntRetweet = contact.cntRetweet;
		double interactionScore = Math.log(1 + 2 * cntReply + cntRetweet);
		LOGGER.info("contactId = " + contact.contactId + ", cntReply: " + cntReply + ", cntRetweet: " + cntRetweet);
		return interactionScore;
	}

	/**
	 * Calculate the number of matched patterns
	 *
	 * @param patterns string pattern
	 * @param match    string for match
	 * @param flag     0 for case sensitive and 1 for case insensitive
	 */
	private static int matchCount(String patterns, String match, int flag) {
		if (flag == 1) {
			// case insensitive
			patterns = patterns.toLowerCase(Locale.ENGLISH);
			match = match.toLowerCase(Locale.ENGLISH);
		}
		Pattern pattern = Pattern.compile(patterns);
		Matcher matcher = pattern.matcher(match);
		int counter = 0;
		int index = 0;
		while (matcher.find(index)) {
			counter++;
			index = matcher.start() + 1;
		}
		LOGGER.info("patterns = " + patterns + ", match = " + match + ", flag = " + flag + ", matchCount = " + counter);
		return counter;
	}

	private static boolean isNullOrEmpty(String[] strs) {
		return strs == null || strs.length == 0;
	}

	private static double getKeywordsScore(Interaction contact, String type, String phrase, String targetHashtag) {
		double keywordsScore = 0;
		String[] tweets = null;
		String[] hashtags = null;
		if ("reply".equals(type)) {
			tweets = contact.allReply;
			hashtags = contact.replyHash;
		} else if ("retweet".equals(type)) {
			tweets = contact.allRetweet;
			hashtags = contact.retweetHash;
		} else if ("both".equals(type)) {
			tweets = contact.allBoth;
			hashtags = contact.bothHash;

		} else {
			LOGGER.error("type is not supported: " + type);
		}

		int phraseMatch = 0;
		int hashtagMatch = 0;

		LOGGER.info("isNullOrEmpty tweets " + isNullOrEmpty(tweets));
		LOGGER.info("isNullOrEmpty hashtags " + isNullOrEmpty(hashtags));

		if (!isNullOrEmpty(tweets) || !isNullOrEmpty(hashtags)) {
			// get phrase match
			if (!isNullOrEmpty(tweets)) {
				for (String tweet : tweets) {
					phraseMatch += matchCount(phrase, tweet, 0);
				}
			}

			// get hashtag match
			if (!isNullOrEmpty(hashtags)) {
				LOGGER.info("hashtags.size = " + hashtags.length);
				for (String hashtagComma : hashtags) {
					for (String hashtag : hashtagComma.split(",")) {
						boolean match = false;
						if (targetHashtag.equalsIgnoreCase(hashtag)) {
							hashtagMatch++;
							match = true;
						}
						LOGGER.info("hashtag" + hashtag + ", targetHash = " + targetHashtag + " => " + match);
					}

				}
			}
			keywordsScore = 1 + Math.log(1 + phraseMatch + hashtagMatch);
		}

		LOGGER.info("contactId = " + contact.contactId + ", phraseMatch: " + phraseMatch + ", hashtagMatch: "
				+ hashtagMatch);
		return keywordsScore;
	}

	private static String getLastTweet(String type, Interaction interaction) {
		String lastTweet = null;
		if ("reply".equals(type)) {
			lastTweet = interaction.lastReply;
		} else if ("retweet".equals(type)) {
			lastTweet = interaction.lastRetweet;
		} else if ("both".equals(type)) {
			lastTweet = interaction.lastBoth;
		} else {
			LOGGER.error("type is not supported: " + type);
		}
		return lastTweet;
	}

}
