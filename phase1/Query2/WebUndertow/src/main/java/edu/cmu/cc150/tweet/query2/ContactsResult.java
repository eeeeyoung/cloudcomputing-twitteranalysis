package edu.cmu.cc150.tweet.query2;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class ContactsResult {
	public UserInfo userInfo;
	public Interaction[] interactions;
	
	/** unit test */
	public static void main(String[] args) {
		Gson gson = new Gson();
		long start = System.currentTimeMillis();
		try {
			ContactsResult result = gson.fromJson(new FileReader("src/main/resources/ContactsSample.json"),
					ContactsResult.class);
			long end = System.currentTimeMillis();
			System.out.println("parsing time millis = " + (end - start));
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}
}
