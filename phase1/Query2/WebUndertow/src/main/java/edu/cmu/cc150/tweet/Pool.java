package edu.cmu.cc150.tweet;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Pool {
	private List<PreparedStatement> ctPsPool = new ArrayList<>();
	private static int INITIAL_POOL_SIZE = 25;
	private static String ctQuery = "SELECT * FROM contacts WHERE uid = ?";

	public Pool(String URL, String MYSQL_NAME, String MYSQL_PWD) throws SQLException {
		for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
			Connection conn = DriverManager.getConnection(URL, MYSQL_NAME, MYSQL_PWD);
			ctPsPool.add(conn.prepareStatement(ctQuery));
		}
	}

	public PreparedStatement getCtPs() throws InterruptedException {
		PreparedStatement ps = null;
		synchronized (ctPsPool) {
			while (ctPsPool.size() == 0) {
				System.out.println("pool too small");
				ctPsPool.wait();
			}
			ps = ctPsPool.remove(ctPsPool.size() - 1);
		}
		return ps;
	}

	public void releaseCtPs(PreparedStatement ps) {
		synchronized (ctPsPool) {
			ctPsPool.add(ps);
			ctPsPool.notifyAll();
		}
	}
}