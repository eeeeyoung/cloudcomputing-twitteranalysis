package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class TwitterDataAnalysis {
    public static void main(String[] args) throws Exception {

        Configuration conf = new Configuration();
        conf.set("mapred.task.timeout", "0");
        Job job = Job.getInstance(conf, "twitter");
        job.setJarByClass(TwitterDataAnalysis.class);
        job.setMapperClass(TwitterMapper.class);
        job.setReducerClass(TwitterReducer.class);

        job.setNumReduceTasks(10);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        MultipleOutputs.addNamedOutput(job, "user", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(job, "interaction", TextOutputFormat.class, Text.class, Text.class);
        MultipleOutputs.addNamedOutput(job, "interactionHbase", TextOutputFormat.class, Text.class, Text.class);
        
        System.exit(job.waitForCompletion(true) ? 0 : 1); // show the progress
    }
}
