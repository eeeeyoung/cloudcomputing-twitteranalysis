package edu.cmu.scs.cc.teamproject;

import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import java.io.IOException;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class TwitterReducer extends Reducer<Text, Text, Text, Text> {

	private MultipleOutputs mOutput;

	@Override
	public void setup(Context context) {
		mOutput = new MultipleOutputs(context);
	}

	@Override
	public void cleanup(Context context) throws IOException, InterruptedException {
		mOutput.close();
	}

	private static String getLatestEntry(Map<Long, String> tM) {
		try {
			for (Map.Entry<Long, String> entry : tM.entrySet()) {
				String value = entry.getValue();
				if (value.equals("")) {
					return "";
				} else {
					return value.replaceAll("teamcc150thebest", "\t");
				}
			}
		} catch (NullPointerException e) {
			// Map not initialized, therefore not exists
			return "";
		}
		return "";
	}
	/**
	 *
	 * @param tM the TreeMap
	 * @return a concatenated string containing every tweet separated by "teamcc150"
	 */
	private static JSONArray getAllEntry(Map<Long, String> tM) {
		JSONArray array = new JSONArray();
		try {
			for (Map.Entry<Long, String> entry : tM.entrySet()) {
				String value = entry.getValue();
				if (value.equals("")) {
					// Do nothing
				} else {
					array.put(value.replaceAll("teamcc150thebest", "\t"));
				}
			}
			return array;
		} catch (NullPointerException e) {
			// Map not initialized, therefore not exists
			return array;
		}
	}

	private static Integer getCounts(ArrayList<String> AL, String targetHashtag) {
		int count = 0;
		for (String ele : AL) {
			if (ele.equals(targetHashtag)) {
				count += 1;
			}
		}
		return count;
	}

	/**
	 *
	 * @param key
	 * @param values
	 * @param con
	 */
	@Override
	public void reduce(Text key, Iterable<Text> values, Context con) {
		Map<String, Integer> countsRetweet = new HashMap<>();
		Map<String, Integer> countsReply = new HashMap<>();
		Set<String> visitedTweets = new HashSet<>();

		// Yang
		ArrayList<Long> duplicateTimeStamp = new ArrayList<>();
		Map<Long, String> screenNames = new TreeMap<Long, String>(Collections.reverseOrder());
		Map<Long, String> descriptions = new TreeMap<Long, String>(Collections.reverseOrder());
		Map<String, TreeMap<Long, String>> peerReplyMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerRetweetMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerBothMap = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerReplyHash = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerRetweetHash = new HashMap<String, TreeMap<Long, String>>();
		Map<String, TreeMap<Long, String>> peerBothHash = new HashMap<String, TreeMap<Long, String>>();
		Set<String> peerSet = new HashSet<>();

		ArrayList<String> totalHashtag = new ArrayList<String>();
		String userId = key.toString().substring(1); // for "i" only
		try {
			for (Text value : values) {
				if (value.toString().trim().isEmpty())
					continue;
				if (key.toString().charAt(0) == 't') {
					// Do nothing, not required in this task
				} else {
					// users table
					if (key.toString().charAt(0) == 'u') {
						String[] secondList = value.toString().split("\t");
						if (secondList.length == 7) {
							// Normal Contact tweet
							Long timeStamp = Long.parseLong(secondList[secondList.length - 2]);
							Long tweetId = Long.parseLong(secondList[secondList.length - 1]);
							if (!duplicateTimeStamp.contains(tweetId)) {
								duplicateTimeStamp.add(tweetId);
								screenNames.put(timeStamp, secondList[1]);
								descriptions.put(timeStamp, secondList[2]);
								if (!secondList[4].equals("None")) {
									peerSet.add(secondList[4]);
								}
								// Testing Code
								for (String tag : secondList[3].split(",")) {
									if (!tag.equals("")) {
										totalHashtag.add(tag);
									}
								}
							}
						} else if (secondList.length == 1) {
							if (!secondList[0].equals("None")) {
								peerSet.add(secondList[0]);
							}
						}
					} else if (key.toString().charAt(0) == 'i') {
						// input for counting interaction
						// key = i + userId
						// value = peerId \t tweetId \t retweetCnt \t replyCnt
						String[] items = value.toString().split("\t");
						if (items.length != 8) {
							// do nothing, it's OK
						} else {
							String peerId = items[0];
							String tweetId = items[1];
							int retweetCnt = "1".equals(items[2]) ? 1 : 0;
							int replyCnt = "1".equals(items[3]) ? 1 : 0;
							if (!visitedTweets.contains(tweetId)) {
								countsRetweet.put(peerId, countsRetweet.getOrDefault(peerId, 0) + retweetCnt);
								countsReply.put(peerId, countsReply.getOrDefault(peerId, 0) + replyCnt);
								visitedTweets.add(tweetId);
								if (items[4].equals("Retweet")) {
									if (!peerRetweetMap.containsKey(items[0])) { // if peer id exists
										peerRetweetMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerBothMap.containsKey(items[0])) {
										peerBothMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerRetweetHash.containsKey(items[0])) {
										peerRetweetHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerBothHash.containsKey(items[0])) {
										peerBothHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									}
									peerRetweetMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
									peerBothMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
									peerRetweetHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
									peerBothHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
								} else if (items[4].equals("Reply")) {
									if (!peerReplyMap.containsKey(items[0])) {
										peerReplyMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerBothMap.containsKey(items[0])) {
										peerBothMap.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerReplyHash.containsKey(items[0])) {
										peerReplyHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									if (!peerBothHash.containsKey(items[0])) {
										peerBothHash.put(peerId, new TreeMap<Long, String>(Collections.reverseOrder()));
									} else {
										assert true;
									}
									peerReplyMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
									peerBothMap.get(peerId).put(Long.parseLong(items[7]), items[5]);
									peerReplyHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
									peerBothHash.get(peerId).put(Long.parseLong(items[7]), items[6]);
								} else {
									// TweetType = "None" just ignore it
									// Only demands latest "contact" tweets, so only considers Retweet and Replies
									assert true;
								}

							}
						}
					}
				}
			}
			// Users table output
			if (key.toString().charAt(0) == 'u') {
				JSONObject userObj = new JSONObject();
				String userU = key.toString().substring(4);
				userObj.put("latestName", getLatestEntry(screenNames));
				userObj.put("latestDesc", getLatestEntry(descriptions));
				StringJoiner hashsj = new StringJoiner(" ");
				if (totalHashtag.size() == 0) {
					userObj.put("censoredHashtags", "");
				} else {
					Set<String> set = new HashSet<String>(totalHashtag);
					for (String hashtag : set) {
						if (!hashtag.trim().equals("")) {
							hashsj.add(hashtag);
							hashsj.add(getCounts(totalHashtag, hashtag).toString());
						}
					}
					userObj.put("censoredHashtags", hashsj.toString());
				}
				JSONArray peersId = new JSONArray();
				for (String peer : peerSet) {
					peersId.put(peer);
				}
				userObj.put("related", peersId);
				mOutput.write("user", new Text("u"+userU), new Text(userObj.toString()));
			}
			// Contacts table output
			// key = userId
			// value = peerId \t  retweetCnt \t replyCnt
			if (key.toString().charAt(0) == 'i' && !key.toString().equals("iNone")) {
				JSONArray peers = new JSONArray();
				for (String peerId : countsRetweet.keySet()) {
					int retweetCnt = countsRetweet.get(peerId);
					int replyCnt = countsReply.get(peerId);

					// for hbase
					JSONObject peer = new JSONObject();
					peer.put("contactId", Long.parseLong(peerId));
					peer.put("cntRetweet", retweetCnt);
					peer.put("cntReply", replyCnt);
					peer.put("lastReply", getLatestEntry(peerReplyMap.get(peerId)));
					peer.put("lastRetweet", getLatestEntry(peerRetweetMap.get(peerId)));
					peer.put("lastBoth", getLatestEntry(peerBothMap.get(peerId)));
					peer.put("allReply", getAllEntry(peerReplyMap.get(peerId)));
					peer.put("allRetweet", getAllEntry(peerRetweetMap.get(peerId)));
					peer.put("allBoth", getAllEntry(peerBothMap.get(peerId)));
					peer.put("replyHash", getAllEntry(peerReplyHash.get(peerId)));
					peer.put("retweetHash", getAllEntry(peerRetweetHash.get(peerId)));
					peer.put("bothHash", getAllEntry(peerBothHash.get(peerId)));
					peers.put(peer);
				}
				if (!peers.isEmpty()) {
					mOutput.write("interactionHbase", new Text("i"+userId), new Text(peers.toString()));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
