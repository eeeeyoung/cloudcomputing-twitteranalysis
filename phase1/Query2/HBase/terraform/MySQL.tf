# terraform init      Initialize a Terraform working directory
# terraform validate  Validates the Terraform files
# terraform fmt       Rewrites config files to canonical format
# terraform plan      Generate and show an execution plan
# terraform apply     Builds or changes infrastructure
# terraform destroy   Destroy Terraform-managed infrastructure

# m5.large database # port 80

provider "aws" {
  region     = "${var.region}"
  shared_credentials_file = "${var.aws-credentials}"
}

resource "aws_security_group" "ssh_database" {
  # inbound internet access
  # allowed: only port 22, 3306 are open
  # you are NOT allowed to open all the ports to the public
  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  # outbound internet access
  # allowed: any egress traffic to anywhere
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }
}

resource "aws_instance" "mysql" {
  ami           = "${var.ami_id}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  availability_zone = "us-east-1a"

  root_block_device {
    volume_type = "gp2"
    volume_size = "100"
  }

  vpc_security_group_ids = [
    # note that id is used, other than name
    "${aws_security_group.ssh_database.id}",
  ]

  tags {
    Project = "${var.project_tag}",
    teambackend = "mysql"
  }
}