# cluster 
[
  {
    "Classification":"hbase-site",
    "Properties": {
       "hbase.snapshot.enabled": "true",
       "hbase.client.keyvalue.maxsize": "0",
       "hbase.server.keyvalue.maxsize": "0"
    }
  },
  {
    "Classification":"mapred-site",
    "Properties": {
       "mapreduce.task.timeout": "0"
    }
  }
]

# hbase shell to db
hbase shell
create 'contacts', 'data'
create 'tweets', 'data'
truncate 'contacts'
truncate 'tweets'

# import data to hbase
refer to HBaseImport module

# GET /q2?user_id=15513&type=retweet&phrase=hello%20cc&hashtag=cmu
http://ec2-35-169-107-234.compute-1.amazonaws.com/q2?user_id=1560513692&type=both&phrase=yuyalarc&hashtag=好きなアルバムあったらrt
http://localhost/q2?user_id=1000089696&type=both&phrase=YaToujoursCeProfQui&hashtag=yatoujoursceprofqui
http://ec2-54-82-26-230.compute-1.amazonaws.com/q2?user_id=1000009578&type=both&phrase=alto%20movimiento&hashtag=mcsuga

# query contacts
get 'contacts', 1000089696
count 'contacts'

