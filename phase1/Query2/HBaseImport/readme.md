# using map reduce to import
mvn clean package 

scp -i /Users/mzhang/git-clones/CloudComputing/ec2_team_keyPair.pem /Users/mzhang/git-clones/CloudComputing/CC150CrackingTheCodingInterview-S19/phase1/Query2/HBaseImport/target/import_tsv.jar hadoop@35.153.126.137:~/

scp -r -i /Users/mzhang/git-clones/CloudComputing/ec2_team_keyPair.pem /Users/mzhang/git-clones/CloudComputing/CC150CrackingTheCodingInterview-S19/phase1/Query2/HBaseImport ubuntu@54.152.133.100:~/

hbase shell
create 'contacts', 'data'

yarn jar import_tsv.jar

nohup yarn jar import_tsv.jar &


# locally import using import tsv
hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns=HBASE_ROW_KEY,data:contacts contacts file:////Users/mzhang/git-clones/CloudComputing/CC150CrackingTheCodingInterview-S19/phase1/Query2/HBase/sample-data/secondrun

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.columns=HBASE_ROW_KEY,data:contacts contacts file:////home/ubuntu/secondrun



# locally import using yetanothertsv
mvn clean package 
hbase