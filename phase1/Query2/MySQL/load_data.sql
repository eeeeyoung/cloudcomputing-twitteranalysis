-- Step 1 create the database
use twitter_db;

-- Step 2 create the tweets table
drop table if exists `contacts`;
create table `contacts` (
    `uid` varchar(70) default "0" not null,
    `data` LONGTEXT
 );

-- Step 3 create an index
create index contact_index ON contacts(uid);

-- Step 4 load data to the tweets table
load data local infile 'contacts.tsv' into table contacts character set utf8mb4 columns terminated by '\t' ESCAPED by '' LINES TERMINATED BY '\n';
