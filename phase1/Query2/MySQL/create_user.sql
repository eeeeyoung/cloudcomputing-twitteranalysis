-- create database twitter_db
drop database if exists twitter_db;
create database twitter_db;

-- create user
create user 'mengmeng'@'localhost' identified by 'dbroot';
grant all privileges on twitter_db.* to 'mengmeng'@'localhost';
create user 'mengmeng'@'%' identified by 'dbroot';
grant all privileges on twitter_db.* to 'mengmeng'@'%';
flush privileges;

