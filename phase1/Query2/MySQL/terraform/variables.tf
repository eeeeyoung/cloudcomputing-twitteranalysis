# the team can share a single root credentials, ask Mengmeng for it
variable "aws-credentials" {
  default = "../../../AWSteamrootkey.csv"
}

# Update "key_name" with the key pair name for SSH connection
# Note: it is NOT the path of the pem file
# the team can share a single pem file, ask Mengmeng for it
# you can find it in https://console.aws.amazon.com/ec2/v2/home?region=us-east-1#KeyPairs:sort=keyName
variable "key_name" {
  default = "ec2_team_keyPair"
}

variable "region" {
  default = "us-east-1"
}

variable "zone" {
  default = "us-east-1a"
}

# instance type
variable "instance_type" {
  default = "m5.large"
}

# Update "project_tag" to match the tagging requirement of the ongoing project
variable "project_tag" {
  default = "Phase1"
}

# Update "ami_id"
variable "ami_id" {
  default = "ami-0ac019f4fcb7cb7e6"
}
