package edu.cmu.cc150.tweet;

import com.google.gson.Gson;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class NewTransaction {
	public static String Team_ID = "CC150CrackingTheCodingInterview";
	public static String account_ID = "384893777434";
	public static String n = "1284110893049";
	public static int e = 15619;
	public static BigInteger d = BigInteger.valueOf(Long.parseLong("379666662787"));
	public static char[] alpnumList = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
			'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}; 
	public static int alpnumLen = alpnumList.length;
	
	public static void stringGen(String prefix, int k, ArrayList<String> prem) {
		if (k == 0) {
			prem.add(prefix); 
			return; 
		} 
		  
		for (int i = 0; i < alpnumLen; ++i) { 
			String newPrefix = prefix + alpnumList[i];  
			stringGen(newPrefix, k - 1, prem);  
		}
	} 
	
	/**
	 * SHA256 - get hash by sha256
	 * 
	 * @param str - str to encrypt
	 * @return hash of str by sha256
	 * @throws NoSuchAlgorithmException
	 */
	public static String SHA256(String str) throws NoSuchAlgorithmException {
		MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
		msgDigest.update(str.getBytes());
		byte[] digest = msgDigest.digest();
		BigInteger bigInt = new BigInteger(1, digest);
		String hex = String.format("%064x", bigInt);
		return hex;
	}
	
	/**
	 * CCHash - get first 8 chars of hash by sha256
	 * 
	 * @param str - str to encrypt
	 * @return first 8 chars of hash by sha256
	 * @throws NoSuchAlgorithmException
	 */
	public static String CCHash(String str) throws NoSuchAlgorithmException {
		return SHA256(str).substring(0, 8);
	}
	
	public static String rsaSig (String transactionHash) {
		BigInteger hashInt = new BigInteger(transactionHash, 16);
		BigInteger nBig = BigInteger.valueOf(Long.parseLong(n));
		BigInteger res = hashInt.modPow(d, nBig);
		return res.toString();
	}
	
	public static String idHashBlock(JSONObject request) {
		int id = 0;
		String hash = "00000000";
		JSONArray chain = request.getJSONArray("chain");
		for (int i = 0; i < chain.length(); i++) {
			JSONObject block = chain.getJSONObject(i);
			id = block.getInt("id");
			hash = block.getString("hash");
		}
	    id++;
	    return Integer.toString(id) + '|' + hash;
	}
	
	public static String getReward(long time) {
		String sendRw = "";
		long timeRw = time + 600000000000L;
		String recvRw = n.toString();
		String amtRw = "500000000";
		String feeRw = "";
		String hsRw = Long.toString(timeRw) + "|" + sendRw + "|" + recvRw + "|" + amtRw + "|" + feeRw;
		String ret = "";
		try {
			ret = CCHash(hsRw);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static String Brute_Force(String powSHA256) {
		for (int l = 1; l < 11; l++) {
			ArrayList<String> prem = new ArrayList<String>();
			stringGen("", l, prem);
			for (int k = 0; k < prem.size(); k++) {
				String hashStr = "";
				try {
					hashStr = CCHash(powSHA256 + prem.get(k));
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (hashStr.charAt(0) == '0') {
					return prem.get(k);
				}
			}	
		}
		return null;
	}
	
	
	public static String addNew(JSONObject request, boolean isValid) {
		String response = Team_ID + "," + account_ID + "\n";
		if (!isValid) {
			response =response + "<INVALID|>";
		} else {
			JSONObject newTx = request.getJSONObject("new_tx");
			String recvTx = Long.toString(newTx.getLong("recv"));
			String amtTx = Long.toString(newTx.getLong("amt"));
			String timeTx = newTx.getString("time");
			String feeTx = "0";
			String sendTx = n;
			String hsTx = timeTx + "|" + sendTx + "|" + recvTx + "|" + amtTx + "|" + feeTx;
			String txHs = "";
			try {
				txHs = CCHash(hsTx);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String sigTx = rsaSig(txHs);
			String powSHAPart = idHashBlock(request);
			String rewardHash = getReward(Long.parseLong(timeTx));
			String powTest = powSHAPart + '|' + txHs + '|' + rewardHash;
			String powSHA256 = "";
			try {
				powSHA256 = SHA256(powTest);
			} catch (Exception e) {
				e.printStackTrace();
			}
			 
			String pow = Brute_Force(powSHA256);
			response =response + "<" + sigTx + "|" + pow + ">";
		}
		return response;
	}
	
	
	public static void main(String[] args) {
		Verification ver = new Verification();
		JSONObject req = ver.base64_decode("eJylkdtKw0AQhl-l7LUXs3PY2fFVRCRJtzZgW2njAaTv7qRVu1EiiLka_mGyH9__Frp102_D9eLmLTQPD3fD63nel-7Zp8iWLWaJCfRqEZrN4KHAx-fJ0G-KRyGKgGK0pGoYWTVi8PW6OazHtVnbFVAKx1tP-6VnUK0BhZRTHk8edy9j1HRD_1zC8WoxBTuU7fIn2AcuG1JOyVDxghuZAJLY-OCqjLSmYLPsymIT9q6JKzE7wR36-_GPhClBjIIJT4D_soXAmHP9IlBqBQwqW7FeA0VoM1S2Dk_tpuzv5319M_MJTMZE7kd8dRFGnIUyXYRFlVl6TMBTeu6QBCtfpClnpSzAcAL8LPHb87-1mLN4L4JfTEyJZqG8bZ2UqMi8orysSoQoxOqgKFy3SJLBGwR05D-0mB174qFrYQWaStUi1po6v4zNsmqxbHZDv9ueD7bl5VziF5iICxMmhXgBmwcyH1M4Ht8BaXrwrw==");
		System.out.println(addNew(req, true));
	}

}
