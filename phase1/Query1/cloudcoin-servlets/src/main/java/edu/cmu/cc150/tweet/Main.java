package edu.cmu.cc150.tweet;

import static io.undertow.servlet.Servlets.defaultContainer;
import static io.undertow.servlet.Servlets.deployment;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import io.undertow.Handlers;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.handlers.PathHandler;
import io.undertow.servlet.Servlets;
import io.undertow.servlet.api.DeploymentInfo;
import io.undertow.servlet.api.DeploymentManager;
import io.undertow.servlet.api.ServletInfo;

/**
 * You should NOT modify this file.
 */
public class Main {

	/**
	 * The constructor.
	 */
	public Main() {
	}

	/**
	 * Base path.
	 */
	public static final String PATH = "/q1";

	/**
	 * Logger.
	 */
	private static final Logger LOGGER = Logger.getRootLogger();

	/**
	 * The main entry of the website.
	 *
	 * @param args run args
	 */
	public static void main(String[] args) {
		try {
			ServletInfo servletInfo = null;
		
			servletInfo = Servlets.servlet("Query1", Query1.class).addMapping("/*");
			
			
			DeploymentInfo servletBuilder = deployment().setClassLoader(Main.class.getClassLoader())
					.setContextPath(PATH).setDeploymentName("handler.war").addServlets(servletInfo);

			DeploymentManager manager = defaultContainer().addDeployment(servletBuilder);
			manager.deploy();

			HttpHandler servletHandler = manager.start();
			PathHandler path = Handlers.path(Handlers.redirect("/q1")).addPrefixPath("/q1", servletHandler);

			Undertow server = Undertow.builder().addHttpListener(80, "0.0.0.0").setHandler(path).build();
			server.start();
		} catch (ServletException e) {
			throw new RuntimeException(e);
		}
	}
}
