#from starlette.requests import Request
#from starlette.responses import PlainTextResponse
#import base64
#import json
#import zlib
#import hashlib
#import re
#from collections import defaultdict
#from itertools import permutations
#from starlette.applications import Starlette
#from starlette.responses import JSONResponse
#import uvicorn
#


def test(cc):
    # ______main_______
    js = base64_decode(cc)
    verif = bc_verification(js)
    a = ADD_NEW(js, verif)
    return a

def q1(request):
    cc = request.query_params['cc']
    # print(cc)
    result = test(cc)
    return PlainTextResponse(result)


alpnum_list = 'abcdefghijklmnopqrstuvwxyz1234567890'




#def Response(sign, PoW, Val):
#    Response = Team_ID + ',' + account_ID + '\n'
#    if Val is True:
#        Response = Response + '<' + str(sign) + '|' + PoW + '>'
##    else:
##        Response = Response + '<INVALID|>'
#
#    return Response



# ADD_NEW: add a new tx to new block
#
# req: json format request
#def ADD_NEW(req, valid):
#
#    else:
#        pow = Brute_Force(pow_SHA256)
#
#        res = Response(sig_tx, pow, True)
#    # print(res)
#    return res

class App:
    def __init__(self, scope):
        assert scope['type'] == 'http'
        self.scope = scope

    async def __call__(self, receive, send):
        request = Request(self.scope, receive)
        cc = request.query_params['cc']
        result = test(cc)
        response = PlainTextResponse(result)
        await response(receive, send)

 if __name__ == '__main__':
     uvicorn.run(app, host='0.0.0.0', port=80)
