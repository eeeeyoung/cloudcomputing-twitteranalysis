from django.urls import path

from . import views

urlpatterns = [
    path('q1/', views.q1, name='index')
]