from django.apps import AppConfig


class CloudcoinAppConfig(AppConfig):
    name = 'cloudcoin_app'
