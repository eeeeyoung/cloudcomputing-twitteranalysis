from django.http import HttpResponse
import base64
import json
import zlib
import hashlib
import re
from collections import defaultdict
from itertools import permutations


def test(cc):
    # ______main_______
    js = base64_decode(cc)
    verif = bc_verification(js)
    a = ADD_NEW(js, verif)
    return a


def q1(request):
    cc = request.GET['cc']
    response = test(cc)
    return HttpResponse(response)


account_ID = '384893777434'
Team_ID = 'CC150CrackingTheCodingInterview'
n = 1284110893049
e = 15619  # Public Key
d = 379666662787  # Private Key
alpnum_list = 'abcdefghijklmnopqrstuvwxyz1234567890'


# Checks if string is Hexidecimal
# Used to check hash
def check_hex(string):
    for ele in string:
        if re.fullmatch(r"^[0-9A-Fa-f]$", ele) is None:
            return False
    else:
        return True


# Checks if the result of the constructed string in each transaction
# matches the hash provided
def checkHash(timestamp, sender, recipient, amount, fee):
    string = str(timestamp)+"|"+str(sender)+"|"+str(recipient)+"|"+str(
        amount)+"|"+str(fee)
    gen_hash = CCHash(string)
    return gen_hash


# Checks if the last hash matches the computed one
def checklastHash(id, previous_block, hash_list, pow):
    string = str(id) + "|" + str(previous_block)
    for ele in hash_list:
        string += "|"+str(ele)
    gen_hash = SHA_256(string)
    return gen_hash + pow


def base64_decode(string):
    jsraw = zlib.decompress(base64.urlsafe_b64decode(string))
    try:
        js = json.loads(jsraw)
    except json.JSONDecodeError:
        return False
    return js


# CCHash: return first 8 chars of sha256
# str: str to encrypt
def CCHash(str):
    sha = SHA_256(str)
    return sha[:8]


# SHA_256: return the SHA-256 result
# str: str to encrypt
def SHA_256(str):
    m = hashlib.sha256()
    b_str = bytes(str, encoding='utf-8')
    m.update(b_str)
    return m.hexdigest()


# HashString: make a string for hash
#
# t: timestamp
# s: sender
# r: recipient
# a: amount of transfer
# f: fee
def HashString(t, s, r, a, f):
    return t + '|' + s + '|' + r + '|' + a + '|' + f


def checkSig(acc, hash1, key, sig):
    hash_int = int(hash1, 16)
    res = pow(sig, e, acc)
    if res == hash_int:
        return True
    else:
        return False


# RSA_SIG: generate signature'
# transactionHash: hex format hash result for tx
def RSA_SIG(transactionHash):
    hash_int = int(transactionHash, 16)
    return pow(hash_int, d, n)


# ID_Hash_Block: get new ID and previous block hash
#
# request: json format request
def ID_Hash_Block(request):
    ID = 0
    Hash = '00000000'
    for block in request.get('chain'):
        ID = block.get('id')
        Hash = block.get('hash')
    ID = ID + 1
    return str(ID) + '|' + Hash


def getReward(time):
    send_rw = ''
    time_rw = time + 600000000000
    recv_rw = str(n)
    amt_rw = '500000000'
    fee_rw = ''
    # time_rw = '1550721967779474176'
    # recv_rw = '34123506233'
    hs_rw = HashString(str(time_rw), send_rw, recv_rw, amt_rw, fee_rw)
    rw_hs = CCHash(hs_rw)
    return rw_hs


def Brute_Force(SHAString):
    for i in range(10):
        String_list = list(permutations(alpnum_list, i))
        for tup in String_list:
            pow = ''
            for char in tup:
                pow = pow + char
            hashStr = CCHash(SHAString + pow)
            if hashStr[0] == '0':
                return pow


def Response(sign, PoW, Val):
    Response = Team_ID + ',' + account_ID + '\n'
    if Val is True:
        Response = Response + '<' + str(sign) + '|' + PoW + '>'
    else:
        Response = Response + '<INVALID|>'

    return Response

# Checks if all previous blocks in the chain are legal
# "chain" stores all transaction lists in the transactions
# transx contains each previous sub-transaction in json format
def bc_verification(json):
    new_tx = json.get("new_tx")
    new_time = new_tx.get("time")
    new_amt = new_tx.get("amt")

    accounts = defaultdict(int)
    chain = json.get("chain")
    timestamps = []
    # Init
    id_counter = 0

    prev_hash = "00000000"
    # main loop
    for transx in chain:
        hashlist = []
        all_tx = transx.get("all_tx")  # Json all_tx
        fees = 0
        for trans in all_tx:
            if len(trans) == 4:  # If it's a reward transaction
                recv = trans.get("recv")  # Integer recv
                amt = trans.get("amt")  # Integer amt
                time = trans.get("time")  # String time
                hash = trans.get("hash")  # String hash
                if len(timestamps) == 0:
                    timestamps.append(time)
                else:
                    if int(time) <= int(timestamps[-1]):
                        print("time error")
                        return False
                if amt > 500000000:
                    return False  # reward amount too big
                accounts[recv] += amt  # reward amount to account
                accounts[recv] += fees
                # Checks if hash of reward transaction is correct
                if checkHash(time, "", recv, amt, "") != hash:
                    return False

            elif len(trans) == 7:  # If it's a normal transaction
                fee = trans.get("fee")  # Integer fee
                send = trans.get("send")  # Integer send
                recv = trans.get("recv")  # Integer recv
                amt = trans.get("amt")  # Integer amt
                time = trans.get("time")  # String time
                hash = trans.get("hash")  # String hash
                sig = trans.get("sig")

                accounts[send] -= fee
                if accounts[send] < 0:
                    return False

                if not checkSig(send, hash, d, sig):
                    return False

                # Checks if hash of reward transaction is correct
                if checkHash(time, send, recv, amt, fee) != hash:
                    return False

                if len(timestamps) == 0:  # checks if time stamps in
                    # ascending order
                    timestamps.append(time)
                else:
                    if int(time) <= int(timestamps[-1]):
                        print("time error")
                        return False
                timestamps.append(time)

                # Checks if fee >= 0
                if fee < 0:
                    return False
                if not check_hex(hash):
                    return False
                # check balance
                accounts[send] -= amt
                if accounts[send] < 0:
                    return False
                accounts[recv] += amt

                fees += fee
            else:
                return False

            hashlist.append(hash)
            # End of each transaction

        id = transx.get("id")
        last_hash = transx.get("hash")
        pow = transx.get("pow")
        hash1 = checklastHash(id, prev_hash, hashlist, pow)
        gen_hash = CCHash(hash1)
        if gen_hash != last_hash:
            return False

        if id != id_counter:
            print("id error")
            return False
        else:
            id_counter += 1

            # Check the PoW
        prev_hash = last_hash
        # End of each transaction list

    # Check if we can pay the final sum

    if int(new_time) < int(timestamps[-1]):
        return False
    if accounts[1284110893049] < new_amt:
        print("no money")
        return False
    return True


# ADD_NEW: add a new tx to new block
#
# req: json format request
def ADD_NEW(req, valid):
    # valid = False
    # with open('request.json') as req:
    if valid is False:
        res = Response(None, None, False)
    else:
        # req is the json format request
        data = req

        # get useful info about new_tx
        new_tx = data.get('new_tx')
        recv_tx = str(new_tx.get('recv'))
        amt_tx = str(new_tx.get('amt'))
        time_tx = new_tx.get('time')
        fee_tx = '0'
        send_tx = str(n)

        #        time_tx = "1550721967779447040"
        #        fee_tx = "5048"
        #        send_tx = "1284110893049"
        #        recv_tx = "484054352161"
        #        amt_tx = "58759591"
        hs_tx = HashString(time_tx, send_tx, recv_tx, amt_tx, fee_tx)
        tx_hs = CCHash(hs_tx)
        sig_tx = RSA_SIG(tx_hs)

        powSHA_part = ID_Hash_Block(data)
        RewardHash = getReward(int(time_tx))

        powtest = powSHA_part + '|' + tx_hs + '|' + RewardHash
        pow_SHA256 = SHA_256(powtest)
        pow = Brute_Force(pow_SHA256)

        res = Response(sig_tx, pow, True)
    # print(res)
    return res


